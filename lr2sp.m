function [sp pp] = lr2sp(LRfile,npsi,ntheta,spdataout,profileout,psin1,diagnostic)
% The conversion function lr2sp generates numerical input files for the 
% GTC simulation code from LamyRidge model equilibria. 
%
% The function has the form:
%
%   [sp pp] = lr2sp(LRfile,npsi,ntheta,spdataout,profileout,diagnostic) ,
%
% where the input parameters are:
%
%   LRfile      - Name of the LamyRidge input equilibrium to be read in, 
%                 stored in a character array.
%   npsi        - Desired radial resolution in the spdata and profile outputs.
%   ntheta      - Desired poloidal resolution in the spdata output.
%   spdataout   - A character array containing a filename, to write generated 
%                 spdata. Will replace existing file.
%   profileout  - A character array containing a filename, to write generated
%                 plasma profile data.  Will replace existing file.
%   diagnostic  - Flag to display diagnostic plots during conversion.
%                   0: do not display diagnostic plots.
%                   1 (or other nonzero): display diagnostic plots.
%
% The output to lr2sp, are GTC spdata and profile data, stored in Matlab struct 
% form as the variables 'sp' and 'pp'. These may be manipulated and plotted, 
% modified, and/or written to file using the scripts writespdat.m and 
% gtcWriteProfile.m respectively.


% apply default input parameter values
if nargin < 7
    if nargin < 6
        if nargin < 5
            if nargin < 4
                if nargin < 3
                    if nargin < 2
                        if nargin < 1
                            error('The first argument must by the name of a LamyRidge input equilibrium.');
                        end
                        npsi = 151;
                    end
                    ntheta = 121;
                end
                spdataout = 'spdata-tmp';
            end
            profileout = 'profile-tmp';
        end
        psin1=10.0
    end
    diagnostic = 1;
end

% some arguments for plots
bwplots=false; %true;
titlefontsize=28;
tickfontsize=20;

%% text open field line file
%LRfile = 'LRstrip.h5'; npsi = 31; ntheta = 121; psin1=10.0;
%spdataout = 'spdata-c2-openfieldline-31x121'; profileout = 'profile-c2-openfieldline-31x121'; 

%% closed field line region
%LRfile = 'symmetric_LRstrip.h5'; npsi = 401; ntheta = 351; psin1=1.0;
%spdataout = 'spdata-c2-boozer-401x351'; profileout = 'profile-c2-boozer-401x351';

%spdataout = 'spdata-tmp'; profileout = 'profile-tmp';
 
%diagnostic = 1;

%%%%% initial parameters and pulling data from C2 model equilibrium %%%%% 
disp('initializing...')
tmp = strsplit(LRfile,'/');
tmp = char(tmp(end));
sp.header = [tmp repmat(' ',1,20-length(tmp)) ' converted with lr2sp.m'];

%   Extent of radial domain in normalized poloidal flux; typically from 0 to 1
%       In these units, 0 is the magnetic axis and 
%       1 is the last closed flux surface.
%   Variable psin0 must ALWAYS be 0.0 to produce consistent spdata files,
%   because convention in spdata assumes our radial grid points start at
%   magnetic axis, however, script will produce points for other ranges.
    psin0 = 0.0;
%    psin1 = 10.0;

%   choose how many theta grids and psi grids
    sp.lsp = npsi;
    sp.lst = ntheta;

%   how much of model equilibrium box should we include?
    %zbound = z(end);
    %zbound = 2.0; % meters
    % zbound determined by finding location where B_r goes to zero...

%   read in the LamyRidge model equilibrium
    disp('reading in model equilibrium...')

    c2eq = readLR(LRfile);
    %c2eq.psi=c2eq.psi*2*pi; % psi is in weber/radian, so DO NOT multiply by 2*pi


%%%%% magnetic coordinate determination %%%%%
    disp('constructing magnetic coordinate mesh...')

%   find the coordinates in  R,Z of the magnetic axis (lowest poloidal flux value)
    disp('finding magnetic axis...')
    [Rmagaxis zmagaxis psi0] = lrGetMagAxis(c2eq);

%   Outermost CLOSED flux surface has polflux value of zero by definition in C2.
%       Any positive valued flux surfaces are open, and any negative surfaces 
%       are closed. (Note this is different from the definition in spdata and 
%       psin quantities above, where value of flux on the magnetic axis is 0, 
%       and increases radially outward, measured in Wb.
    psi1 = -psi0*(psin1-1.0);

%   dummies; filler values for spdata
    sp.lemax=4;
    sp.lrmax=8;

%   Set value of flux on outer surface in spdata file (in spdata
%   poloidal flux defined such that there is zero flux on mag axis).
    sp.psiw = psi1-psi0; % last flux surface
    sp.ped = -psi0; % last CLOSED flux surface

%   choose psi values for GTC input files (C2 flux definition)
    pp.psi = -psi0*((0:1/(sp.lsp-1):1)'*(psin1-psin0) + psin0 - 1.0);
%   convenient flags for inside/outside separatrix parts
    flclosed = 0;  flopen = 0;
    if psin0 < 1.0
        flclosed = 1;
    end
    if psin1 > 1.0
        flopen = 1;
    end
%   index label for last closed flux surface (useful later)
    if flclosed
        iLastClosed = max(find(pp.psi<=0));
    else
        iLastClosed = 0;
    end

%   find location where B_r --> 0 for z-boundary.
    % contour of the separatrix
    separatrix = contourc(c2eq.z,c2eq.r,c2eq.psi,[0 0]);
    separatrix = separatrix(:,2:end);
    % only do this if we have open field lines
    if(flopen)
      % contour of outermost open field line flux surface
      psi1contour = contourc(c2eq.z,c2eq.r,c2eq.psi,[psi1 psi1]);
      psi1contour = psi1contour(:,2:end);
      % value of B_R along the outer contour
      bbrpsi1 = interp2(c2eq.z,c2eq.r,c2eq.bbr,psi1contour(1,:),psi1contour(2,:));

      % function that returns B_R(z) on the outermost flux surface
      bbrsp = @(z) interp1(psi1contour(1,:),bbrpsi1,z,'spline');

      % want to find 'zeros' in bbrsp, but want to make a good estimate first...
      % find first change in sign of B_R in the +z direction, outside of the X-points
      % tmpind is first index after the sign change
      tmpind = min(find(c2eq.z>max(separatrix(1,:)) & sign(bbrsp(c2eq.z))~=sign(bbrsp(c2eq.z(min(find(c2eq.z>0)))))));
      % now search for B_R==0 on flux surface psi1, in a range just above and below index indicated by tmpind
      zbound = fzero(bbrsp,c2eq.z(tmpind-2),c2eq.z(tmpind+1));
    else % closed flux surface only
      % set zbound 5% wider than separatrix
      zbound = 1.05*max(separatrix(1,:));
    end
    % print out for diagnostic
    disp(['zbound is +/- ' num2str(zbound)])


%   cleanup    
    clear psin0 psin1 psi1

%   choose regularly spaced theta values for GTC input files
    theta = (0:(sp.lst-1))*(2*pi/(sp.lst-1)); % for output
    tt = (0:(sp.lst*5-1))*(2*pi/(5*sp.lst-1)); % denser grid for better interpolation
    tt(end) = 2*pi;


%%% straight field line coordinates
disp('determining straight field line coordinates...')

%   pull cartesian coordinates from LamyRidge eq. and then
%   translate coordinates so magnetic axis is at origin
    R = c2eq.r - Rmagaxis;
    z = c2eq.z - zmagaxis;

%%%%%%%%%%%%%%%%

if flclosed
disp('straight field line coords for CLOSED flux surfaces...')
%   generate contour for outer CLOSED flux surface,
%   so irrelevant if you have open field lines only 
    c = contourc(z,R,c2eq.psi,[pp.psi(iLastClosed) pp.psi(iLastClosed)]);
%   just want z,R points, so trim other information off
    c = c(:,2:end);

%   Compute corresponding thetas from z,R and store in row 3.
%   Uses arctan(z/R) and then corrects output so in range from 0 to 2pi
    c(3,:) = atan(c(1,:)./c(2,:))+(c(2,:)<0)*pi+(c(1,:)<0).*(c(2,:)>=0)*2*pi;

%   sort points, so they are strictly monotonic in theta.  
%   first sort columns by ascending theta
    c = sortrows(c',3)';
%   now find the 0/2pi point and append to front and back
    c(:,2:end+1) = c;  % move all points back one space
    c(:,[1 end+1]) = 0.0; % initialize first point and new last point to zero
    c(1,[1 end]) = 0.0; % z-value is same as magnetic axis
    % find the x value which matches desired flux value most closely given z=zmagaxis
    %c(2,[1 end]) = fminunc(@(x)interp2(z,R,(c2eq.psi-pp.psi(iLastClosed)).^2,0.0,x,'cubic'),max(c(2,2:end-1)));
    c(2,[1 end]) = fminsearch(@(x)interp2(z,R,(c2eq.psi-pp.psi(iLastClosed)).^2,0.0,x,'cubic'),max(c(2,2:end-1)));
    c(3,1) = 0; % angle set to 0
    c(3,end) = 2*pi; % angle set to 2*pi

%   then make sure there are no duplicate thetas
    c = c(:,[find(diff(c(3,:))) size(c,2)]);

%   Generate some r values along lines defined by constant theta.

%   First find a suitable estimate of maximum r for each constant theta ray:
%   Find closest point on contour of outer most flux surface
    rmaxInd = round(interp1(c(3,:),1:size(c,2),tt));
%   Compute r of this point, and add a little bit to make sure we're just 
%   outside this flux surface.
    rmax = sqrt(c(1,rmaxInd).^2 + c(2,rmaxInd).^2)*1.05;
%   Double check we're not crossing outside of the cartesian mesh
    tmp = -(cos(tt)<0).*(Rmagaxis./cos(tt))+(cos(tt)>0).*(rmax+1.0);
    rmax = min([rmax ; tmp]);
%   cleanup    
    clear tmp c rmaxInd

%   for each constant theta ray...
textprogressbar('closed field line region : ')
for i=1:sp.lst*5
    %disp(['closed field line region: ' num2str(i) ' of ' num2str(sp.lst*5)])
    textprogressbar(20*i/sp.lst)
    % used frequently...
    cost = cos(tt(i));
    sint = sin(tt(i));

    % for each theta we have a maximum distance from mag axis to sample some
    % poloidal flux values.  for sensibility, sampling points should be
    % regular in r and have greater resolution as we are asking for
    % in psi, in order to resolve desired psi points well via interpolation.
    r = (0:(1/(5*iLastClosed)):1)*rmax(i);
    
    % now do 2D interpolation on the cartesian grid to get psi on these r's
    psir = interp2(z,R,c2eq.psi,r*sint,r*cost,'spline');
    psir(1) = pp.psi(1);

    % do 1D interpolation to extract the correct r values back
    rpt = interp1(psir,r,pp.psi(1:iLastClosed),'pchip');
        
    % compute z,R from rpt and theta
    xf(1:iLastClosed,i) = rpt*cost;
    zf(1:iLastClosed,i) = rpt*sint;

    clear cost sint r psir rpt
end
textprogressbar(' DONE.')
end
    clear i rmax
%%%%%%%%%%%%%%%%

keepind = find(abs(z)<=zbound);
if flopen
disp('straight field line coords for OPEN flux surfaces...')

    %   bottom boundary...
    bottom = contourc(z(keepind),R,c2eq.psi(:,keepind),[-0.000001*psi0 -0.000001*psi0]);
    %   top boundary...    
    top = contourc(z(keepind),R,c2eq.psi(:,keepind),[pp.psi(end) pp.psi(end)]);

    %   only interested in z,R points, so trim
    bottom = bottom(:,2:end);
    top = top(:,2:end);

    %   convert z to theta_f
    bottom(1,:) = (bottom(1,:)/zbound + 1.0)*pi;
    top(1,:) = (top(1,:)/zbound + 1.0)*pi;    
    %bottom(1,:) = mod(pi*bottom(1,:)/zbound,2*pi);
    %top(1,:) = mod(pi*top(1,:)/zbound,2*pi);

    %   sort points, so they are strictly monotonic in theta.  
    %   first sort columns by ascending theta
    bottom = sortrows(bottom',1)';
    top = sortrows(top',1)';
    %   use first point as thetaf = 0,2pi point (duplicates removed in next step)
    bottom(:,2:end+1) = bottom;
    bottom(1,1) = 0;
    bottom(:,end+1) = bottom(:,2);
    bottom(1,end) = 2*pi;
    top(:,2:end+1) = top;
    top(1,1) = 0;
    top(:,end+1) = top(:,2);
    top(1,end) = 2*pi;
    %   then make sure there are no duplicate thetas, 
    %       if we duplicated the 0 or 2pi points, they will be erased here.
    bottom = bottom(:,[find(diff(bottom(1,:))) size(bottom,2)]);
    top = top(:,[find(diff(top(1,:))) size(top,2)]);

    %   finally, for each theta, find minimum and maximum R values for psi scan
    Rlower = max(R(1), interp1(bottom(1,:),bottom(2,:),tt));
    Rupper = min(R(end), interp1(top(1,:),top(2,:),tt));

    %   cleanup
    clear bottom top

    %z positions corresponding to interp thetas
    zz = zbound*(tt/pi - 1.0);
    %zz = zbound*(mod((tt/pi)+1,2)-1);
    textprogressbar('open field line region   : ')
    %   for each theta...
    for i=1:sp.lst*5
        %disp(['open field line region: ' num2str(i) ' of ' num2str(sp.lst*5)])    
        textprogressbar(20*i/sp.lst)
        % For each theta we have an R range which flux surfaces may be inside.
        %   For sensibility, sampling points should be regular in r and 
        %   have greater resolution as we are asking for in psi, 
        %   in order to resolve desired psi points well via interpolation.
        r = Rlower(i):(Rupper(i)-Rlower(i))/(5*(npsi-iLastClosed)):Rupper(i);
            
        % now do 2D interpolation on the cartesian grid to get psi on these r's
        psir = interp2(z,R,c2eq.psi,zz(i),r,'spline');
        
        % do 1D interpolation to extract the correct r values back
        rpt = spline(psir,r,pp.psi(iLastClosed+1:end));
                
        % compute z,R from rpt and theta
        xf(iLastClosed+1:length(pp.psi),i) = rpt;
        zf(iLastClosed+1:length(pp.psi),i) = zz(i);

        % clean up
        clear r psir rpt
    end
    textprogressbar(' DONE.')
end


   [tmp minzind] = min(zf(end,:));
% if diagnostics are on, plot the straight field line coordinates
if 0 %diagnostic
    fh = figure; hold on;
    if flclosed
        plotmesh(zf(1:iLastClosed,:),...
                 xf(1:iLastClosed,:),...
                 max(round(npsi/50),1),...
                 max(round(5*ntheta/100),1),...
                 1,'-k');
    end
    if flopen
        plotmesh(zf(iLastClosed+1:end,[minzind:end 1:minzind-1]),...
                 xf(iLastClosed+1:end,[minzind:end 1:minzind-1]),...
                 max(round(npsi/50),1),...
                 max(round(5*ntheta/100),1),...
                 0,'-k');
    end
    set(fh,'position',[200 450 1400 450])
    title('straight field line coord.')
end
disp('straight field line coordinates are determined.')


disp('computing boozer coordinates from straight field line coordinates...')
% The periodic part of the magnetic scalar potential is 
% required to compute boozer coordinates. We will obtain this value
% from the covariant form of the B-field.

% Compute covariant theta_f (straight field line theta) component of B, 
% on dense straight field line coordinates.
%
%    |B x grad(rho)|/|grad(rho) x grad(theta_f)|
%
% Here rho is our radial coordinate, poloidal flux.
% Doing this cross product in cartesian coordinates,
% because we have cartesian B components from LR eq file.

disp('     computing covariant theta_f component of B on straight field line grid...')
% to get (x,z) components of straight field line coordinate gradients 
%   (grad(rho) and grad(thetaf), we solve a linear system of equations by expressing grad(x)
%   and grad(z) in terms of straight field line gradients.

% first compute partial derivatives on the straight field line grid


%   dx/drho
    dxdp = zeros(size(xf));
    % most flux surfaces can be done with simple differencing
    dxdp(2:end-1,:) = midpts(diff(xf,1,1),1)./repmat(midpts(diff(pp.psi)),[1 size(xf,2)]);
    if flclosed
        % singular on axis, set to zero
        dxdp(1,:)=0;
        dxdp(iLastClosed,:)=dxdp(iLastClosed-1,:);
    end
    if flopen
        % assume dxdp constant at last flux surface
        dxdp(end,:)=dxdp(end-1,:);
        dxdp(iLastClosed+1,:)=dxdp(iLastClosed+2,:);
    end

%   dz/drho
    dzdp = zeros(size(zf));
    % most flux surfaces can be done with simple differencing
    dzdp(2:end-1,:) = midpts(diff(zf,1,1),1)./repmat(midpts(diff(pp.psi)),[1 size(zf,2)]);
    if flclosed
        % singular on axis, set to zero
        dzdp(1,:)=0;
        dzdp(iLastClosed,:)=dzdp(iLastClosed-1,:);
    end
    if flopen
        % assume dzdp constant at last flux surface
        dzdp(end,:)=dzdp(end-1,:);
        dzdp(iLastClosed+1,:)=dzdp(iLastClosed+2,:);
    end

%   dx/dtheta_f
    dxdt = zeros(size(xf));
    % periodic okay for x; 
    % actually periodic for closed flux surface, 
    % and symmetric (same x both ends) for open
    dxdt = midpts(diff(xf(:,[end-1 1:end 2]),1,2),2)./repmat(midpts(diff(tt([end-1 1:end 2]))), [size(xf,1) 1]);

%   dz/dtheta_f
    dzdt = zeros(size(zf));
    % first set all as periodic 
    dzdt = midpts(diff(zf(:,[end-1 1:end 2]),1,2),2)./repmat(midpts(diff(tt([end-1 1:end 2]))), [size(zf,1) 1]);
    % need to overwrite periodic derivative for any open field lines
    if flopen
        dzdt(iLastClosed+1:end,1)=dzdt(iLastClosed+1:end,2);
        dzdt(iLastClosed+1:end,end)=dzdt(iLastClosed+1:end,end-1);
    end
%    if iLastClosed < length(pp.psi)
%        dzdt(iLastClosed+1:end,minzind) = dzdt(iLastClosed+1:end,minzind+1);
%        dzdt(iLastClosed+1:end,minzind-1) = dzdt(iLastClosed+1:end,minzind-2);
%    end


% Now, use linear algebra to solve for cartesian components of 
%   grad(rho) and grad(thetaf)
    warning('off','MATLAB:singularMatrix')
    for i = 1:size(dxdp,1);
        for j = 1:size(dxdp,2);
            %   matrix
                A = [[dxdp(i,j) 0 dxdt(i,j) 0];...
                     [0 dxdp(i,j) 0 dxdt(i,j)];...
                     [dzdp(i,j) 0 dzdt(i,j) 0];...
                     [0 dzdp(i,j) 0 dzdt(i,j)]];
            %   resulting vector (represents x,z components of grad(x)=[1 0] and grad(z)=[0 1]
                b = [1 0 0 1]';

            %   solve A*soln = b
                soln = linsolve(A,b);
                gradrhox(i,j) = soln(1);
                gradrhoz(i,j) = soln(2);
                gradtfx(i,j) = soln(3);
                gradtfz(i,j) = soln(4);
                clear A b soln;
        end
    end
    warning('on','all')

% We already have cartesian components of the B-field, but interpolate them onto
%   the straight field line grid
    bbrf = interp2(z,R,c2eq.bbr,zf,xf);
    bbzf = interp2(z,R,c2eq.bbz,zf,xf);
%   force B_r to be zero at ends of open field lines...
%      this is just numerical housekeeping.
%      insufficient if B_R is far from zero.
    bbrf(iLastClosed+1:end,[1 end])=0.0;

% Take cross product to determine grad(thetaf) component of B-field!
    bthetaff = abs(-bbrf.*gradrhoz + bbzf.*gradrhox)./abs(-gradrhox.*gradtfz + gradrhoz.*gradtfx);
    % some NaNs may crop up from div-by-zero, cover known BCs...
    % B is zero on magnetic axis
    if flclosed
        bthetaff(1,:) = 0;
    end

% make sure we still have symmetry about z=0 plane
bthetaff = .5*(bthetaff + bthetaff(:,end:-1:1));

% if doing diagnostics, plot this out to make sure it's reasonable
%{
if diagnostic
    figure
    if flclosed
%        contourf(zf(1:iLastClosed,:),xf(1:iLastClosed,:),bthetaff(1:iLastClosed,:),100,'linestyle','none')
        surface(zf(1:iLastClosed,:),xf(1:iLastClosed,:),bthetaff(1:iLastClosed,:),'linestyle','none')
        hold on
        plotmesh(zf(1:iLastClosed,:),xf(1:iLastClosed,:),npsi/10,ntheta*5/20,0,'m')
    end
    if flopen
%        contourf(zf(iLastClosed+1:end,:),xf(iLastClosed+1:end,:),bthetaff(iLastClosed+1:end,:),100,'linestyle','none')
        surface(zf(iLastClosed+1:end,:),xf(iLastClosed+1:end,:),bthetaff(iLastClosed+1:end,:),'linestyle','none')
        hold on
        plotmesh(zf(iLastClosed+1:end,:),xf(iLastClosed+1:end,:),npsi/10,ntheta*5/20,0,'m')
    end
    title('B_{\theta f}')
    colorbar
end
%}


disp('    applying FFT to separate const. and periodic components...')
clear i bb cc dd tmp
bb = fft(bthetaff,[],2);

% get constant part, and save as current constant
cc = bb*0;
cc(:,1) = bb(:,1);
tmp = real(ifft(cc,[],2));
muitor = tmp(:,1);
clear tmp;

% integrate fourier components to get mag scalar potential
bb(:,1) = 0;
tmp = bb;
tmp(:,2:end) = -sqrt(-1)*bb(:,2:end)./repmat(1:size(bb,2)-1,size(bb,1),1);
phitil = real(ifft(tmp,[],2));
% enforce that phitil = 0 at thetaf=thetaB=0
phitil(:,1)=0;
clear tmp

if 0 %diagnostic
    fluxind = floor(npsi/2);
    figure
    subplot(3,1,1)
    plot(bthetaff(fluxind,:))
    xlabel('\theta_i')
    ylabel('per. mag. scalar. pot.')
    subplot(3,1,2)
    plot(real(bb(fluxind,:)))
    hold on
    plot(imag(bb(fluxind,:)),'m')
    xlabel('k_{\theta}')
    subplot(3,1,3)
    tmp = real(ifft(bb,[],2));
    plot(tmp(fluxind,:))
    hold on
    plot(phitil(fluxind,:),'m')
    plot(xlim,[0 0],'k')
    xlabel('\theta_i')
    clear fluxind
end


disp('    determining theta_f values on regular theta_B grid and...')
disp('    interpolating x,z values to regular theta_B grid...')
clear i

xfsp = conspl2d(xf,sp.psiw,2*pi);
zfsp = conspl2d(zf,sp.psiw,2*pi,0,0);
indx = 1:sp.lsp;
delx = 0.0*indx;

indstart = 1+flclosed;
for i = indstart:sp.lsp
    tb = tt + phitil(i,:)/muitor(i);
    tb(end) = 2*pi;
    thetafatB = interp1(tb,tt,theta);
%    indy = floor(thetafatB/(tt(2)-tt(1)))+1;
%    dely = thetafatB + (tt(2)-tt(1))*(1 - indy);
%    sp.xsp(i,:,1) = gtcSpline2d(xfsp,i,indy,0,dely);
%    sp.zsp(i,:,1) = gtcSpline2d(zfsp,i,indy,0,dely);
    sp.xsp(i,:,1) = spline(tt,xf(i,:),thetafatB);
    sp.zsp(i,:,1) = spline(tt,zf(i,:),thetafatB);
end

% show relationship between theta_f and theta_B on a reference flux surface
if diagnostic
    figure('position',[60 400 645 605],...
           'paperposition',[0 0 6.45 6.05],...
           'papersize',[6.45 6.05])
    i = flclosed+1;
    plot(tt/pi,(tt + phitil(i,:)/muitor(i))/pi,'.k')
    xlabel('\theta/\pi',...
           'units','normalized',...
           'position',[.99 0],...
           'horizontalalignment','right',...
           'verticalalignment','bottom',...
           'fontsize',titlefontsize)
    ylabel('\theta_B /\pi',...
           'units','normalized',...
           'position',[0 .99],...
           'horizontalalignment','right',...
           'verticalalignment','top',...
           'fontsize',titlefontsize)
    set(gca,'fontsize',tickfontsize)
    grid on
    grid minor
    xlim([0 2])
    ylim([0 2])
    title('\theta_B vs \theta',...
          'fontsize',titlefontsize)
end 

    

clear indstart

bmagn = sqrt(c2eq.bbr.^2 + c2eq.bbt.^2 + c2eq.bbz.^2);
sp.bsp = interp2(z,R,bmagn,sp.zsp(:,:,1),sp.xsp(:,:,1),'spline');
% force B_R= zero at end of open field line (not sufficient, if not already smooth and close to zero)
sp.bsp(:,[1 end]) = interp2(z,R,sqrt(c2eq.bbt.^2 + c2eq.bbz.^2),sp.zsp(:,[1 end],1),sp.xsp(:,[1 end],1),'spline');
sp.gsp = repmat(muitor,1,ntheta)./(2*pi*sp.bsp(:,:,1).^2);

% change coordinates back to original origin
sp.xsp = sp.xsp + Rmagaxis;
sp.zsp = sp.zsp + zmagaxis;

sp.xsp = conspl2d(sp.xsp(:,:,1),sp.psiw,2*pi,0,0);
sp.zsp = conspl2d(sp.zsp(:,:,1),sp.psiw,2*pi,0,0);
sp.bsp = conspl2d(sp.bsp(:,:,1),sp.psiw,2*pi,0,0);
sp.gsp = conspl2d(sp.gsp(:,:,1),sp.psiw,2*pi,0,0);

sp.qpsi = zeros(sp.lsp,3); % safety factor zero by definition in C2
% current functions
if flclosed
    Rpsi(1:iLastClosed) = sp.xsp(1:iLastClosed,1,1);
end
if flopen
    Rpsi(iLastClosed+1:sp.lsp) = sp.xsp(iLastClosed+1:end,ceil(sp.lst/2),1);
end
Rpsi = Rpsi';

% third, interpolate desired quantities on the flux grid
%btpsi = spline(c2eq.psi(ri_min:end,zi_min),c2eq.bbt(ri_min:end,zi_min),pp.psi);
btpsi = interp2(c2eq.z,c2eq.r,c2eq.bbt,zmagaxis,Rpsi,'cubic');
sp.gpsi = zeros(sp.lsp,3); % by definition in C2
sp.gpsi(:,1) = Rpsi.*btpsi;

%%%%%%%%%%%%%%%%%%%%%%%
sp.rd = zeros(sp.lsp,3);
sp.rd(:,1)=muitor;


clear btpsi

% toroidal flux
sp.torpsi = zeros(sp.lsp,3); % zero by definition in C2

% minor radius
sp.rpsi = zeros(sp.lsp,3);
sp.rpsi(:,1) = Rpsi - Rmagaxis;
sp.rpsi = gtcConstSpline1d(sp.rpsi(:,1),sp.psiw);

% things for the profile.dat
pp.x =sqrt(sp.torpsi(:,1)); %sqrt normalize tor. flux
pp.r = sp.rpsi(:,1); % minor radius
pp.R(1:sp.lsp,1) = Rmagaxis;
pp.rpR = pp.r + pp.R;

% pressure
sp.ppsi = zeros(sp.lsp,3); % not used in gtc
sp.ppsi(:,1)=1;
pp.te = interp2(c2eq.z,c2eq.r,c2eq.tme,zmagaxis,Rpsi,'cubic');
pp.ne = interp2(c2eq.z,c2eq.r,c2eq.en,zmagaxis,Rpsi,'cubic');
pp.ti = interp2(c2eq.z,c2eq.r,c2eq.tm01,zmagaxis,Rpsi,'cubic');

pp.zeff = zeros(sp.lsp,1); pp.zeff(1)=1;
pp.omegator = zeros(sp.lsp,1);  pp.omegator(1)=1;
pp.Er = zeros(sp.lsp,1);
pp.ni = interp2(c2eq.z,c2eq.r,c2eq.en01,zmagaxis,Rpsi,'cubic');

sp.ppsi(:,1) = (pp.ne.*pp.te + pp.ni.*pp.ti)*1.6e-19; % plasma pressure in J/m^3
sp.ppsi = gtcConstSpline1d(sp.ppsi(:,1),sp.psiw);

% major radius
sp.rmaj = Rmagaxis;

pp.nimp = zeros(sp.lsp,1); pp.nimp(1)=1;
pp.nf = zeros(sp.lsp,1); pp.nf(1)=1;
pp.tf = zeros(sp.lsp,1); pp.tf(1)=1;

clear ri_min zi_min i;

% more dummies
sp.krip = 0;
sp.nrip = 0;
sp.d0 = 0;
sp.brip = 0;
sp.wrip = 0;
sp.xrip = 0;


if diagnostic
% plot straight field line and boozer coordinate meshes
    fh = figure;
    set(fh,'position',[40 200 950 815])
    if flopen
        set(fh,'position',[40 200 1500 900])
    end

    spp = gensubplotpos(3,1,0,.02,.08,.01,.09,.03);

    % first plot, cylindrical coordinates
    subplot('position',spp.subplotpos(1,1:4))
    hold on
    contourf(z,R,-c2eq.psi/psi0,[-1:.01:0 0:.1:(-sp.psiw/psi0)-1],'linestyle','none')
    cax = caxis;
    customcolmap = interp1(-1:2/(length(colormap)-1):1,...
                     colormap,...
                    [ -1:1/(.5*length(colormap)*abs(cax(1))-1):0 ...
                       0:1/(.5*length(colormap)*abs(cax(2))-1):1 ]);
    colormap(customcolmap)
    [tmpr tmpz] = meshgrid(R,z(keepind));
    plotmesh(tmpz,...
             tmpr,...
             max(round(length(keepind)/50),1),...
             max(round(c2eq.nr/22),1),...
             0,'-k')
    [tmpC tmph] = contour(z,R,c2eq.psi,[0 1e-10],'-k','linewidth',3);
    clear tmpr tmpz tmph cax customcolmap
    xlim([-1 1]*1.05*max(tmpC(1,2:tmpC(2,1)+1)))
    ylim([-0.34 0.14])
    if flopen
        xlim([z(keepind(1)) z(keepind(end))])
        ylim([-0.34 0.4])
    end
    %title('cylindrical coordinates',...
    title('(R, Z)',...
          'units','normalized',...
          'position',[.99 .99],...
          'horizontalalignment','right',...
          'verticalalignment','top',...
          'fontsize',titlefontsize,...
          'backgroundcolor','w')
    set(gca,...
        'xticklabel','',...
        'fontsize',tickfontsize)
    ylabel('R - R_0 (m)')

    % second plot, straight field line coordinates
    %subplot(2,1,1)
    subplot('position',spp.subplotpos(2,1:4))
    hold on
    if flclosed
        plotmesh(zf(1:iLastClosed,:),xf(1:iLastClosed,:),...
                 max(round(npsi/50),1),...
                 max(round(5*ntheta/100),1),...
                 1,'-k')
        xlim([-1 1]*1.05*max(tmpC(1,2:tmpC(2,1)+1)))
        ylim([-0.34 0.14])
    end
    if flopen
        %plotmesh(zf(iLastClosed+1:end,:),xf(iLastClosed+1:end,:),npsi/10,5*ntheta/40)
        plotmesh(zf(iLastClosed+1:end,[minzind:end 1:minzind-1]),...
                 xf(iLastClosed+1:end,[minzind:end 1:minzind-1]),...
                 max(round(npsi/50),1),...
                 max(round(5*ntheta/100),1),...
                 0,'-k')
    xlim([z(keepind(1)) z(keepind(end))])
    ylim([-0.34 0.4])
    end
    contour(c2eq.z,c2eq.r-Rmagaxis,c2eq.psi,[0 1e-10],'-k','linewidth',3)
    %title('straight field line coordinates',...
    title('(\psi_f, \theta_f)',...
          'units','normalized',...
          'position',[.99 .99],...
          'horizontalalignment','right',...
          'verticalalignment','top',...
          'fontsize',titlefontsize)
%    xlim([z(1) z(end)])
    set(gca,'xticklabel','',...
        'fontsize',tickfontsize)
    ylabel('R - R_0 (m)')

    % third plot, boozer coordinates
%    subplot(2,1,2)
    subplot('position',spp.subplotpos(3,1:4))
    hold on
    [tmp spzminind] = min(sp.zsp(end,:,1));
%   plotmesh(sp.zsp(1:iLastClosed,:,1),sp.xsp(1:iLastClosed,:,1),npsi/10,ntheta/40)
%   plotmesh(sp.zsp(iLastClosed+1:end,:,1),sp.xsp(iLastClosed+1:end,:,1),npsi/10,ntheta/40)
    if flclosed
        plotmesh(sp.zsp(1:iLastClosed,:,1),sp.xsp(1:iLastClosed,:,1),...
                 max(round(npsi/50),1),...
                 max(round(ntheta/100),1),...
                 1,'-k')
        xlim([-1 1]*1.05*max(tmpC(1,2:tmpC(2,1)+1)))
        ylim([-0.34 0.14]+Rmagaxis)
    end
    if flopen
        plotmesh(sp.zsp(iLastClosed+1:end,:,1),...
                 sp.xsp(iLastClosed+1:end,:,1),...
                 max(round(npsi/50),1),...
                 max(round(ntheta/100),1),...
                 0,'-k')
        xlim([z(keepind(1)) z(keepind(end))])
        ylim([-0.34 0.4]+Rmagaxis)
    end
    contour(c2eq.z,c2eq.r,c2eq.psi,[0 1e-10],'-k','linewidth',3)
    %title('Boozer coordinates',...
    title('(\psi_B, \theta_B)',...
          'units','normalized',...
          'position',[.99 .99],...
          'horizontalalignment','right',...
          'verticalalignment','top',...
          'fontsize',titlefontsize)
    set(gca,...
        'fontsize',tickfontsize)
    xlabel('Z (m)')
    ylabel('R (m)')
    clear tmpC spp
end % grid progression: cylindrical ---> straight field ---> boozer

%%%%%%%% b-field + grid overlay
if diagnostic
    figure
    contourf(c2eq.z,c2eq.r,bmagn,200,'linestyle','none')
    caxis([0 0.1])
    colorbar
    hold on
    [tmp spzminind] = min(sp.zsp(end,:,1));
%   plotmesh(sp.zsp(1:iLastClosed,:,1),sp.xsp(1:iLastClosed,:,1),npsi/10,ntheta/40)
%   plotmesh(sp.zsp(iLastClosed+1:end,:,1),sp.xsp(iLastClosed+1:end,:,1),npsi/10,ntheta/40)
    if flclosed
        plotmesh(sp.zsp(1:iLastClosed,:,1),sp.xsp(1:iLastClosed,:,1),...
                 max(round(npsi/50),1),...
                 max(round(ntheta/100),1),...
                 1,'-k')
    end
    if flopen
        plotmesh(sp.zsp(iLastClosed+1:end,:,1),...
                 sp.xsp(iLastClosed+1:end,:,1),...
                 max(round(npsi/50),1),...
                 max(round(ntheta/100),1),...
                 0,'-k')
    end
    title('Boozer coordinate grid + B-field magnitude (T)')
    xlabel('z (m)')
    ylabel('R (m)')
%   xlim([z(1) z(end)])
    xlim([z(keepind(1)) z(keepind(end))])
    ylim([0 0.6])
end % b-field + grid overlay

%%% jacobian
%if bwplots 
%    lr2spjacbw
%else
%    lr2spjaccol
%end

% done with interpolation... shift pol flux so that magnetic axis has polflux=0
pp.psi = pp.psi-psi0;

% sometimes routine is finicky depending on resolution... check for NaNs...
% function checks how many nans in a multidimensional data structure
nansum = @(x)(sum(isnan(reshape(x,numel(x),1))));
spfields = fieldnames(sp);
for i = 1:length(spfields)
  if nansum(sp.(char(spfields(i))))~=0
    error('lr2sp:NaNError',['NaNs found in ' char(spfields(i)) ' field of spline data...\nTry different spdata resolution.\naborting'])
  end
end
ppfields = fieldnames(pp);
for i = 1:length(ppfields)
  if nansum(pp.(char(ppfields(i))))~=0
    error('lr2sp:NaNError',['NaNs found in ' char(ppfields(i)) ' field of profile data...\nTry different spdata resolution.\naborting'])
  end
end
clear spfields ppfields

disp('writing spdata to file...')
writespdat(sp,spdataout)

disp('writing profile to file...')
gtcWriteProfile(pp,profileout)

%clear R Rmagaxis Rpsi c2eq cost i r ri_min rmax rmaxInd sint theta ...
%      z zi_min zmagaxis Rgrid zgrid

disp('all done!')


