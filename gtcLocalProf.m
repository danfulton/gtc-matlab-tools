function gtcLocalProf(inputProfile, location, inner, outputProfile)
% function gtcLocalProf(inputProfile, location, outputProfile)
% Selects a location in a full plasma profile, and reproduces the profile such
% that it has flat gradients (i.e. profiles are exponential) from psi_n=inner:1, 
% and the single value of the gradient is taken from specified location.
% Between psi_n=0:inner, the profile (not gradient) will be flat.  
%
% Location should be given in terms of normalized poloidal flux.
%
% Requires that the input plasma profile has correct information on the minor
% radius (not normalized) and at least the value of the on-axis major radius.
%
if nargin < 4
  if nargin < 3
   	if nargin < 2
			error('Input profile name and location are required arguments.');
	  end
    inner=0;
  end
	outputProfile = [inputProfile '_' num2str(location) '_flxtb'];
end

if (location < 0 || 1 < location)
	error('Location should be specified in normalized poloidal flux, and must be between 0 and 1.');
end	

% read in profile, and store in convenient variables
	pp = gtcReadProfile(inputProfile);

	% poloidal flux
	ppsi = pp.PolFlux;
	% plasma temp and density profiles
	f = [pp.ne pp.ni pp.Te pp.Ti pp.ne+pp.ni];
	% minor radius in (cm)
    r = pp.r;


% find indices on either side of chosen location, and then take the gradient here	

%	clear tmp;
%	tmp = ppsi - repmat(location*ppsi(end),length(ppsi),1);	
	ind(2) = min(find(ppsi/ppsi(end)>location));
	ind(1) = ind(2)-1;
	
	fgrad = diff(log(f(ind,:)))/diff(r(ind))

% artificial scaling of gradients. normally, fgrad should not be changed.
% order is [pp.ne pp.ni pp.Te pp.Ti pp.ne+pp.ni]
%  fgrad = .1*[1 1 1 1 1].*fgrad
%  fgrad = [1 1 .5 .5 1].*fgrad

% We want the local value of the profile quantity, f(location), to be
% the "on-axis" value for our new profile.  Grab local value here.

	rloc = spline(ppsi,r,location*ppsi(end));
	floc = interp1(ppsi,f(:,1:4),location*ppsi(end));
%	coef = floc./exp(fgrad(:,1:4)*r(1));

% rewrite our new profile as an exponential	
	clear f;
%	f = repmat(coef,length(r),1).*exp(r*fgrad(1:4));
	f = exp(r*fgrad(1:4));

% flatten this profile inside of psi_n=inner to avoid exponential blow up
% around psi_n = 0.
  indInner = min(find(ppsi/ppsi(end)>inner));
  f(1:indInner-1,:)=repmat(f(indInner,:),indInner-1,1);

% normalize, so on axis value is same as local value from original profile
  coef = floc./f(1,:);
  f = f.*repmat(coef,length(f),1);

% put modified profiles back in the struct, and save struct to new file.
	pp.ne = f(:,1);
	pp.ni = f(:,2);
	pp.Te = f(:,3);
	pp.Ti = f(:,4);
	
	gtcWriteProfile(pp,outputProfile)

% Some useful information for the user.
disp('')
disp(['Profile written to ' outputProfile]);
disp('')
disp('Local profile values: ')
disp(['minor radius (cm) = ' num2str(rloc)])
disp(['ne (m^-3)=' num2str(floc(1),'%e')])
disp(['ni (m^-3)=' num2str(floc(2),'%e')])
disp(['Te (eV)=' num2str(floc(3),'%e')])
disp(['Ti (eV)=' num2str(floc(4),'%e')])
disp(['omega star = m*dt/B0 * ' num2str(-fgrad(5)*100*sqrt(1.67e-27 *floc(3)*1.6e-19)*pp.R(1)/(rloc*1.6e-19), '%e')])
disp('   with dt in GTC units and B0 in Tesla.')
% done
