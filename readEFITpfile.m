function A = readEFITpfile(pfile)
% function readEFITpfile reads in the columns of an EFIT profile
% and stores them in a struct, A.
% Assumes that the length of sampling is 256 points, and that
% there are five quantities to read in.  Also requires a
% 'clean' pfile, with text lines deleted.

if nargin < 1
		pfile = 'tmp.txt';
end

% Three columns of data for quantity Q:
% psinorm, Q, dQ/dpsi

clear tmp A
tmp = load(pfile);
i=0;
A.ne = tmp(i*256+1:(i+1)*256,:);
i=1;
A.te = tmp(i*256+1:(i+1)*256,:);
i=2;
A.ni = tmp(i*256+1:(i+1)*256,:);
i=3;
A.ti = tmp(i*256+1:(i+1)*256,:);
i=4;
A.omeg = tmp(i*256+1:(i+1)*256,:);
clear tmp
