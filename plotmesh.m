function h = plotmesh(X,Z,rskip,oskip,periodic,varargin)
% function h = plotmesh(X,Z,rskip,oskip,periodic,varargin)
%
% Plots a flat mesh in cartesian space specified by two dimensional
% arrays X and Z.  Lines are drawn along the dimensions of X and Z, sampling
% every rskip and oskip times in the radial and theta directions.

clear tmp_xs tmp_zs tmp_i tmp_washeld

% Verify user input.
	tmp_xs = size(X);
	tmp_zs = size(Z);

	if ((length(tmp_xs) ~= 2) || (length(tmp_zs) ~= 2))
   		echo 'Input must be two dimensional arrays.'
   		return
	end

	if sum(tmp_xs ~= tmp_zs)
   		echo 'Input arrays must be the same size.'
   		return
	end

    if (nargin < 5)
        periodic = 0;
    	if (nargin < 4)
   	    	oskip = 1;
   		    if (nargin < 3)
   	  		    rskip = 1;
    	  		if (nargin < 2)
	    	 		echo 'Must supply arrays to create mesh.'
	         		return
	  		    end
       		end
	    end
    end

	rskip = floor(rskip);
	oskip = floor(oskip);

	if (rskip < 1) || (oskip < 1)
   		disp('rskip and oskip must be integer values >= 1.')
   		return
	end

% Duplicate first angle line at end of array to guarantee
% plot covers full 2*pi radians.
if periodic
  X(:,end+1) = X(:,1);
  Z(:,end+1) = Z(:,1);
end

% Plot the mesh.

	%Check the current hold state.
	tmp_washeld = ishold;

	%Plot one contour first, in case we are replacing the old plot...
	htmp = plot(X(1,:),Z(1,:),varargin{:});
	%... then turn on hold on so we can successively plot the whole mesh.
	hold on
	for tmp_i=1+rskip:rskip:tmp_xs(1)
		plot(X(tmp_i,:),Z(tmp_i,:),varargin{:})
	end
	% Make sure we get the outside contour.
	plot(X(end,:),Z(end,:),varargin{:})
	for tmp_i=1:oskip:tmp_xs(2)
		plot(X(:,tmp_i),Z(:,tmp_i),varargin{:})
	end
	
	% should we pass the function handle out?
	if nargout > 0
	    h = htmp;
	end

	%If hold was off before, turn it off again.
	if(~tmp_washeld) 
		hold off
	end

% end of function plotmesh
