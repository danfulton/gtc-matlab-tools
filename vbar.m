function ah=vbar(x,varargin)

if nargin < 1
  error('function vbar requres a scalar or vector input to specify locations for vertical bar plot')
end

if ~isvector(x)
  error('function vbar requres input in scalar or vector form')
end

isheld = ishold;
hold on

ax = axis;
for i=1:length(x)
  plot([1 1]*x(i), ax(3:4),varargin{:})
end

if(~isheld)
  hold off
end
