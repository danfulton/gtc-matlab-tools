function gtcFlattenProf(inputProfile, maxLevel, outputProfile)

if nargin < 3
	if nargin < 2
		error('Input profile name and gradient cutoff are required arguments.');
	end
	outputProfile = [inputProfile '-flat'];
end

% read in profile, and store in more convenient variables
	pp = gtcReadProfile(inputProfile);

	%ppsi = pp.PolFlux;
	%ppsi_half = .5*(ppsi(1:end-1)+ppsi(2:end));
    r = pp.r;
    r_half = .5*(r(1:end-1)+r(2:end));
	f = [pp.ne pp.ni pp.Te pp.Ti];

% determine the gradients
	dlnfdr = -diff(log(f))./repmat(diff(r),1,4);

% for each profile
%	1) find flux value, loc, where gradient is equal to user specified max level
%	2) find appropriate scaling coefficient, s.t. coef*exp(-level*psi) is 
%		equal to the value of the profile at loc
%	3) find index of the first psi value past loc
%	4) rewrite profile of f past found index, with fitted exponential
	for i = 1:4
		% make sure variables are cleaned up first.
		clear loc coef tmp ind
		% 1)
		loc = interp1(dlnfdr(:,i),r_half,maxLevel);
		% 2)
		coef = interp1(r,f(:,i),loc)/exp(-maxLevel*loc);
		% 3)
		tmp = r-repmat(loc,length(r),1);
		ind = min(find(tmp>0));
		% 4)
		f(ind:end,i) = coef*exp(-maxLevel*r(ind:end));
	end

% put modified profiles back in the struct, and save struct to new file.
	pp.ne = f(:,1);
	pp.ni = f(:,2);
	pp.Te = f(:,3);
	pp.Ti = f(:,4);
	
	gtcWriteProfile(pp,outputProfile)

% done


