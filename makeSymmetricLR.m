function makeSymmetricLR(origLRfile)

if nargin < 1
    error('Function makeSymmetricLR(origLRfile) requires a file name for reading!');
end


fileExtInd = find(origLRfile=='.');
if isempty(fileExtInd)
    % no file extension found in the file name
    error('The filename supplied in origLRFile has no file extension!')
else
    % create new file name
    fileExtInd = fileExtInd(end);
    newFile = [origLRfile(1:fileExtInd-1) '-symmetric' origLRfile(fileExtInd:end)];
end

% read the whole file as a struct
a = h5ToStruct(origLRfile);

% need to find the magnetic axis, i.e. the minimum point in magnetic flux, psi
% first, verify we have fields we need.  these are r,z,psi
% also verify these fields have expected relative sizes

if ~isfield(a,'psi')
    error('No psi field found in the input data file.  Check file contents with h5info() or h5disp().');
end
if ~isfield(a,'r')
    error('No r field found in the input data file.  Check file contents with h5info() or h5disp().');
end
if ~isfield(a,'z')
    error('No z field found in the input data file.  Check file contents with h5info() or h5disp().');
end
if ~(size(a.psi,1)==length(a.r) && size(a.psi,2)==length(a.z))
    errmsg =  ['Size mismatch! Size of r is ' num2str(length(a.r)) 'x1,' ...
               ' size of z is 1x' num2str(length(a.z)) ',' ...
               ' and size of psi is ' num2str(size(a.psi,1)) 'x' num2str(size(a.psi,2)) '.'];
    error(errmsg);
end
% fields are verified

nr = length(a.r);
nz = length(a.z);
leftind = 1:floor(nz/2);
rightind = leftind+nz-floor(nz/2);

% make z perfectly symmetric
a.z = .5*(a.z-a.z(end:-1:1));
% for each field of struct, reflect and mirror it
attrs = fieldnames(a);
for i=1:length(attrs) % for each field in the struct
    group='/mesh/';    
    if (size(a.(attrs{i}))==[nr nz]) % if 2D array, mirror appropriately
        group='/state/';
        leftsum = sum(sum(a.(attrs{i})(:,leftind)));
        rightsum = sum(sum(a.(attrs{i})(:,rightind)));
        % check parity
        if sign(leftsum)==sign(rightsum) % sign is same
            a.(attrs{i}) = .5*(a.(attrs{i})(:,:)+a.(attrs{i})(:,end:-1:1));
        else % sign is opposite
            a.(attrs{i}) = .5*(a.(attrs{i})(:,:)-a.(attrs{i})(:,end:-1:1));
        end
    end
%    right now, all 1D quantities belong to "mesh", others are "state"    
%    if this changes, we can implement isMeshQuantity, which explicitly checks field names
%    if isMeshQuantity(attrs{i})
%        group='/mesh/'
%    else
%        group='/state/'
%    end
    % all field in struct gets written back out to hdf5 file
    h5create(newFile,[group attrs{i}],size(a.(attrs{i})))
    h5write(newFile,[group attrs{i}],a.(attrs{i}))
end

disp(['Symmetric file ' newFile ' has been generated.'])
disp('Done!')

%{
% if R0,Z0 is significantly off center grid point, need to be more careful and generate new points
% find (r,z) coordinate location of minimum psi
    % minimum point value
    [tmp zi_min] = min(min(a.psi));
    [tmp ri_min] = min(a.psi(:,zi_min));
    clear tmp
    % root finding, using min point as first guess
    [tmp psi0] = fminunc(@(x)interp2(a.z,a.r,a.psi,x(1),x(2),'cubic'),[a.z(zi_min) a.r(ri_min)]);
    zmagaxis = tmp(1);
    Rmagaxis = tmp(2);
    clear tmp
%}
