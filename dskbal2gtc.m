function sp=dskbal2gtc(dskbalFilename, spdataFilename, profileFilename)

if nargin < 3
  if nargin < 2
    if nargin < 1
      error('Must provide a filename for are dskbal file to convert!');
    end
    % default spdata filename
    spdataFilename = 'spdata.tmp';
  end
  % default profile filename
  profileFilename = 'profile.tmp';
end

dsk = readdskbal(dskbalFilename);

sp.lsp = dsk.npsi;
sp.lst = dsk.nthet;
sp.lemax = 0;
sp.lrmax = 0;
sp.psiw = dsk.psi(end)-dsk.psi(1);
sp.ped = sp.psiw;

%splines not included yet, need to construct gtc splines.

% 2D splines
% compute bfield
%%% b_toroidal can be taken from F with minimal work
btor = repmat(dsk.f,[1 dsk.nthet])./dsk.x;

%%% need to do some math to determine b_poloidal
%%% derivatives w.r.t. psi
% copy psi values by nthet, so array is same size as x,z
tmppsi = repmat(dsk.psi,[1,dsk.nthet]);
% for most points, derivative is simple
dxdpsi = diff(midpts(dsk.x))./diff(midpts(tmppsi));
dzdpsi = diff(midpts(dsk.z))./diff(midpts(tmppsi));
% for end points, use derivative for last segment, and predict using 2nd deriv. at end-1 point.
tmpdxdpsi = diff(dsk.x(end-2:end,:))./diff(tmppsi(end-2:end,:));
tmpdzdpsi = diff(dsk.z(end-2:end,:))./diff(tmppsi(end-2:end,:));
tmpdpsi = diff(tmppsi(end-2:end,:));

dxdpsi(end+1,:) = tmpdxdpsi(2,:)+tmpdpsi(2,:).*(diff(tmpdxdpsi)./midpts(tmpdpsi));
dzdpsi(end+1,:) = tmpdzdpsi(2,:)+tmpdpsi(2,:).*(diff(tmpdzdpsi)./midpts(tmpdpsi));

%%% derivatives w.r.t theta
% evenly spaced in theta
dthet = 2*pi/(dsk.nthet-1);
% most derivatives are straightforward
dxdthet = diff(midpts(dsk.x,2),1,2)/dthet;
dzdthet = diff(midpts(dsk.z,2),1,2)/dthet;
% first and last points in theta direction are duplicates, b.c. should be periodic, so deriv. still straightforward
bcdxdthet = (dsk.x(:,2)-dsk.x(:,end-1))/(2*dthet);
bcdzdthet = (dsk.z(:,2)-dsk.z(:,end-1))/(2*dthet);
% append to ends of other points, so we have theta derivatives on 2D flux grid
dxdthet = [bcdxdthet dxdthet bcdxdthet];
dzdthet = [bcdzdthet dzdthet bcdzdthet];

dxdthet = dxdthet(2:end,:);
dzdthet = dzdthet(2:end,:);

denom = 1./(dxdpsi.*dzdthet - dzdpsi.*dzdthet);

bpol = sqrt((dzdthet.*dzdthet + dxdthet.*dxdthet).*denom.*denom).*repmat(dsk.dchidpsi(2:end),[1,dsk.nthet])./dsk.x(2:end,:);
bpol = [zeros(1,dsk.nthet); bpol];

%%% ^^^ is b_poloidal, but atm the units seem to have some problem

%sp.bsp = sqrt(btor.*btor + bpol.*bpol);
sp.bsp = btor;

% x and z coordinates
sp.xsp = dsk.x;
sp.zsp = dsk.z;

sp.gsp = 0; %fix this

% 1D splines
sp.qpsi = dsk.q;

% work these out carefully w/ maxwells eqns
sp.gpsi = dsk.f;
sp.rd = dsk.x(:,1).*dsk.pcoldp + dsk.ffprime./dsk.x(:,1);

sp.ppsi = dsk.ne.*(dsk.te + dsk.ti);
sp.rpsi = dsk.x(:,1)-dsk.x(1,1);
% this one is probably wrong
sp.torpsi = dsk.dchidpsi;

end % of function dskbal2gtc
