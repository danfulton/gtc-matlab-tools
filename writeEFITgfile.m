function writeEFITgfile(A,gfile)

% write in to a long string, so that later we can parse for exponential format,
% changing normalization from Matlab style [1,10) to Fortran style [0,1).

filestr = '';

% write data

%     header, pest parameter, # horizontal R grid points, # vertical z grid points
%      READ(kueql,1000)(etitl(i),i=1,5),date,ipestg,nx,nz
      % make sure header is 48 characters
      tmpcase = horzcat(A.case, repmat(' ',1,48));
      tmpcase = tmpcase(1:48);
      filestr = [filestr sprintf('%c',tmpcase)];
      filestr = [filestr sprintf('%4d',A.idum)];
      filestr = [filestr sprintf('%4d',A.nw)];
      filestr = [filestr sprintf('%4d',A.nh)];
      filestr = [filestr sprintf('\n')];

%     horz size (m) of computational box, vert size (m) of computational box, R of B location, R of boundary, middle z value
%      READ(kueql,1010) xdim,zdim,rcnt,redge,zmid
      filestr = [filestr sprintf('%16.9E', A.rdim)];
      filestr = [filestr sprintf('%16.9E', A.zdim)];
      filestr = [filestr sprintf('%16.9E', A.rcentr)];
      filestr = [filestr sprintf('%16.9E', A.rleft)];
      filestr = [filestr sprintf('%16.9E', A.zmid)];
      filestr = [filestr sprintf('\n')];

%     major radius (at magnetic axis), z at mag axis, polflux at mag axis, polflux at limiter, B at specified R location (see above)
%      READ(kueql,1010) xma,zma,psimax,psilim,btor
      filestr = [filestr sprintf('%16.9E', A.rmaxis)];
      filestr = [filestr sprintf('%16.9E', A.zmaxis)];
      filestr = [filestr sprintf('%16.9E', A.simag)];
      filestr = [filestr sprintf('%16.9E', A.sibry)];
      filestr = [filestr sprintf('%16.9E', A.bcentr)];
      filestr = [filestr sprintf('\n')];

%     current in Amps, 
%      READ(kueql,1010) totcur,psimx(1),psimx(2),xax(1),xax(2)
      filestr = [filestr sprintf('%16.9E', A.current)];
      filestr = [filestr sprintf('%16.9E', A.simag)];
      filestr = [filestr sprintf('%16.9E', A.xdum)];
      filestr = [filestr sprintf('%16.9E', A.rmaxis)];
      filestr = [filestr sprintf('%16.9E', A.xdum)];
      filestr = [filestr sprintf('\n')];

%      READ(kueql,1010) zax(1),zax(2),psisep,xsep,zsep
      filestr = [filestr sprintf('%16.9E', A.zmaxis)];
      filestr = [filestr sprintf('%16.9E', A.xdum)];
      filestr = [filestr sprintf('%16.9E', A.sibry)];
      filestr = [filestr sprintf('%16.9E', A.xdum)];
      filestr = [filestr sprintf('%16.9E', A.xdum)];
      filestr = [filestr sprintf('\n')];
      
      %poloidal current function... R*B_toroidal (Wb/m)
%      READ(kueql,1010) (sf(i),     i = 1,npv)
      filestr = [filestr sprintf('%16.9E%16.9E%16.9E%16.9E%16.9E\n', A.fpol)];
      if mod(A.nw,5)
        filestr = [filestr sprintf('\n')];
      end

      %plasma pressure in Joule/m^3
%      READ(kueql,1010) (sp(i),     i = 1,npv)
      filestr = [filestr sprintf('%16.9E%16.9E%16.9E%16.9E%16.9E\n', A.pres)];
      if mod(A.nw,5)
        filestr = [filestr sprintf('\n')];
      end

      %f df/dpsi
%      READ(kueql,1010) (sffp(i),   i = 1,npv)
      filestr = [filestr sprintf('%16.9E%16.9E%16.9E%16.9E%16.9E\n', A.ffprim)];
      if mod(A.nw,5)
        filestr = [filestr sprintf('\n')];
      end

      % dp/dpsi
%      READ(kueql,1010) (spp(i),    i = 1,npv)
      filestr = [filestr sprintf('%16.9E%16.9E%16.9E%16.9E%16.9E\n', A.pprime)];
      if mod(A.nw,5)
        filestr = [filestr sprintf('\n')];
      end

      % polflux on rectangular grid
%      READ(kueql,1010) ((psi(i,j), i = 1,nx), j = 1,nz)
      filestr = [filestr sprintf('%16.9E%16.9E%16.9E%16.9E%16.9E\n', A.psizr)];
      if mod(A.nw*A.nh,5)
        filestr = [filestr sprintf('\n')];
      end

      % q values on flux surfaces
%      READ(kueql,1010) (efitq(i),  i = 1,npv)
      filestr = [filestr sprintf('%16.9E%16.9E%16.9E%16.9E%16.9E\n', A.qpsi)];
      if mod(A.nw,5)
        filestr = [filestr sprintf('\n')];
      end

      % number of boundary points, and number of limiter points
%      READ(kueql,1020) neftbd,neftlm
      filestr = [filestr sprintf('%5d', A.nbbbs)];
      filestr = [filestr sprintf('%5d', A.limitr)];
      filestr = [filestr sprintf('\n')];

      % plot the boundary
%      READ(kueql,1010) (efitbdy(i),i = 1,nefbdy)
      filestr = [filestr sprintf('%16.9E%16.9E%16.9E%16.9E%16.9E\n', [A.rbbbs; A.zbbbs])];
      if mod(2*A.nbbbs,5)
        filestr = [filestr sprintf('\n')];
      end
      
      % plot the limiter
%      READ(kueql,1010) (efitlim(i),i = 1,neflim)
      filestr = [filestr sprintf('%16.9E%16.9E%16.9E%16.9E%16.9E\n', [A.rlim; A.zlim])];
      if mod(2*A.limitr,5)
        filestr = [filestr sprintf('\n')];
      end

%     check some switches, and characteristic radius of rotation
      filestr = [filestr sprintf('%5d', A.kvtor)];
      filestr = [filestr sprintf('%16.9E', A.rvtor)];
      filestr = [filestr sprintf('%5d', A.nmass)];
      filestr = [filestr sprintf('\n')];

%     rotation related quantities; optional
      if A.kvtor > 0
        filestr = [filestr sprintf('%16.9E%16.9E%16.9E%16.9E%16.9E\n', A.pressw)];
        if mod(A.nw,5)
            filestr = [filestr sprintf('\n')];
        end
        filestr = [filestr sprintf('%16.9E%16.9E%16.9E%16.9E%16.9E\n', A.pwprim)];
        if mod(A.nw,5)
            filestr = [filestr sprintf('\n')];
        end
      end

%     mass related properties; optional
      if A.nmass > 0
        filestr = [filestr sprintf('%16.9E%16.9E%16.9E%16.9E%16.9E\n', A.dmion)];
        if mod(A.nw,5)
            filestr = [filestr sprintf('\n')];
        end
      end

%     toroidal flux
      filestr = [filestr sprintf('%16.9E%16.9E%16.9E%16.9E%16.9E\n', A.rhovn)];
      if mod(A.nw,5)
        filestr = [filestr sprintf('\n')];
      end

% open file for writing
findex = fopen(gfile,'w');
if findex < 0 %error
    error(['Could not open the gfile ', gfile]);
end

% write the whole string, using proper exponential normalization
% function normEstr uses regular expression replacement to find and operate on 
% strings that appear to represent numbers in exponential format
fprintf(findex,'%c',normEstr(filestr));

% close the file
fclose(findex);


