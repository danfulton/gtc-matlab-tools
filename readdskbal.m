function a=readdskbal(filename)

%%%%% argument checks

if nargin < 1
  error('You must supply a filename as an argument.')
end

%%%%% open file

findex = fopen(filename,'r');
if findex < 0
  error(['Could not open the dskbal file ', filename])
end

%%%%% read in data!!!

% header
for tmpi=1:90
  fgetl(findex);
end

%%% geometry data

% flux grid dimensions: number of flux surfaces, and theta coordinates
fgetl(findex);
a.npsi = fscanf(findex,'%i',1);
a.nthet = fscanf(findex,'%i',1);
fgetl(findex);

% poloidal flux
fgetl(findex);
a.psi = fscanf(findex,'%e',a.npsi);
fgetl(findex);
fgetl(findex);
a.pcoldp = fscanf(findex,'%e',a.npsi);
fgetl(findex);

% poloidal current function
% f = R*B_toroidal
fgetl(findex);
a.f = fscanf(findex,'%e',a.npsi);
fgetl(findex);
% ffprime = R * B_toroidal * (d/dpsi)(R*B_toroidal)
fgetl(findex);
a.ffprime = fscanf(findex,'%e',a.npsi);
fgetl(findex);

% related to toroidal flux?
fgetl(findex);
a.dchidpsi = fscanf(findex,'%e',a.npsi);
fgetl(findex);

% safety factor, q
fgetl(findex);
a.q = fscanf(findex,'%e',a.npsi);
fgetl(findex);

% spatial coordinates, X(psi,theta), Z(psi,theta)
fgetl(findex);
a.x = fscanf(findex,'%e',[a.npsi,a.nthet]);
fgetl(findex);
fgetl(findex);
a.z = fscanf(findex,'%e',[a.npsi,a.nthet]);
fgetl(findex);

%%% plasma parameter data

% electron density (cm^-3)
fgetl(findex);
a.ne = fscanf(findex,'%e',a.npsi);
fgetl(findex);

% gradient of electron density (d/dpsi ne)
fgetl(findex);
a.neprime = fscanf(findex,'%e',a.npsi);
fgetl(findex);

% electron temperature (eV)
fgetl(findex);
a.te = fscanf(findex,'%e',a.npsi);
fgetl(findex);

% gradient of electron temperature (d/dpsi te)
fgetl(findex);
a.teprime = fscanf(findex,'%e',a.npsi);
fgetl(findex);

% ion temperature (eV)
fgetl(findex);
a.ti = fscanf(findex,'%e',a.npsi);
fgetl(findex);

% ion temperature gradient (d/dpsi ti)
fgetl(findex);
a.tiprime = fscanf(findex,'%e',a.npsi);
fgetl(findex);

%%%%% close file

fclose(findex);

end % of function readdskbal
