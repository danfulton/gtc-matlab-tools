function B = midpts(A,dim)

if nargin < 2
  if nargin < 1
    error('Requires an array with length of at least 2.')
  end
  dim = 1;
end

if length(A) < 2
    error('Length of input array must be >= 2.')
end

tmpA = A;

flipbool=0;

if ndims(A) == 2
  if dim == 1
    if size(A,1)==1
      tmpA=A';
      flipbool=1;
    end
  end
end

  if dim ~= 1
    order = 1:ndims(A);
    order(1) = dim;
    order(dim) = 1;
    tmpA = permute(A,order);
  end

  if ndims(A) > 2
    sizeB = size(tmpA);
    sizeB(1) = sizeB(1)-1;
    tmpA = tmpA(1:end,:);
  end

  B = .5*(tmpA(2:end,:)+tmpA(1:end-1,:));

  if ndims(A) > 2
    B=reshape(B,sizeB);
  end

  if dim ~= 1
    B = ipermute(B,order);
  end

if flipbool==1
  B = B';
end

end
