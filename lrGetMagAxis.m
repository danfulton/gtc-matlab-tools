function [r0 z0 psi0] = ancGetMagAxis(lrstruct)

    clear  tmp zi_min ri_min

    [tmp zi_min] = min(min(lrstruct.psi));
    [tmp ri_min] = min(lrstruct.psi(:,zi_min));
    clear tmp 

    %[tmp psi0] = fminunc(@(x)interp2(lrstruct.z,lrstruct.r,lrstruct.psi,x(1),x(2),'cubic'),[lrstruct.z(zi_min) lrstruct.r(ri_min)]);
    [tmp psi0] = fminsearch(@(x)interp2(lrstruct.z,lrstruct.r,lrstruct.psi,x(1),x(2),'cubic'),[lrstruct.z(zi_min) lrstruct.r(ri_min)]);

    z0 = tmp(1);
    r0 = tmp(2);
    clear tmp zi_min ri_min
