holdflag = ishold;

hold on
plot(xlim,[0 0],'-k')
plot([0 0],ylim,'-k')

if ~holdflag
    hold off
end

clear holdflag
