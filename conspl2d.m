function spl = conspl2d(z,dim1size,dim2size,dim1periodic,dim2periodic)
% function spl = gtcConstSpline2d(z,dim1size, dim2size)

if nargin < 5
    if nargin < 4
        dim1periodic = 0;
    end
    dim2periodic = 1;
end

disp('initializing...')


% verify user input
% z is two dimensional
if ndims(z)~=2
    error('Input grid must be two dimensional.');
end
% odd number of theta points

% change odd number and first last, not the same or even and same
if mod(size(z,2),2)==0
    error('Input grid must have an odd number of points in 2nd dim.')
end

lsp = size(z,1);
lst = size(z,2);

dpx = dim1size/(lsp-1);
dtx = dim2size/lst;



disp('creating intermediary grid...')

% create an intermediate grid with 4-pt averaging.  We need an intermediate grid,
% in order to calculate quadratic polynomials using 9 grid points that are
% ***centered*** on the current grid cells.  
% Boundary conditions: for periodic conditions, we can still just use 
% 4pt averaging, boundaries are no different from the center of the grid.  
% For fixed BC, make a guess based on finite difference.
clear zmid
zmid = zeros(size(z)+2);  %+2
% in between grid points, four point avg
zmid(2:end-2,2:end-2)=midpts(midpts(z,1),2);

% at corners, four cases to consider
if dim1periodic & dim2periodic
tmp = 0.25*(z(1,1) + z(1,end) + z(end,1) + z(end,end));
zmid(1,end-1) = tmp;
zmid(end-1,1) = tmp;
zmid(end-1,end-1) = tmp;
elseif dim1periodic
zmid(end-1,1) = mean((7*z([1 end],1)-4*z([1 end],2)+z([1 end],3))/4);
tmp = mean((7*z([1 end],end)-4*z([1 end],end-1)+z([1 end],end-2))/4);
zmid(1,end-1) = tmp;
zmid(end-1,end-1) = tmp;
clear tmp
elseif dim2periodic
zmid(1,end-1) = mean((7*z(1,[1 end])-4*z(2,[1 end])+z(3,[1 end]))/4);
tmp = mean((7*z(end,[1 end])-4*z(end-1,[1 end])+z(end-2,[1 end]))/4);
zmid(end-1,1) = tmp;
zmid(end-1,end-1) = tmp;
clear tmp
% else, this point not important, so leave it as zero.
end

% for side points, only two cases to consider, if periodic or not
if dim1periodic
tmp = midpts(midpts(z([end 1],:),1),2);
zmid(1,2:end-2) = tmp;
zmid(end-1,2:end-2) = tmp;
clear tmp
else
zmid(1,2:end-2) = midpts((7*z(1,:)-4*z(2,:)+z(3,:))/4);
zmid(end-1,2:end-2) = midpts((7*z(end,:)-4*z(end-1,:)+z(end-2,:))/4);
end
if dim2periodic
tmp = midpts((7*z(:,1)-4*z(:,2)+z(:,3))/4);
tmp = midpts(midpts(z(:,[1 end]),1),2);
zmid(2:end-2,1) = tmp;
zmid(2:end-2,end-1) = tmp;
clear tmp
else
zmid(2:end-2,1) = midpts((7*z(:,1)-4*z(:,2)+z(:,3))/4);
zmid(2:end-2,end-1) = midpts((7*z(:,end)-4*z(:,end-1)+z(:,end-2))/4);
end

if dim1periodic
zmid(end,2:end-1) = zmid(2,2:end-1);
% else these points don't matter, so leave as zero
end
if dim2periodic
zmid(2:end-1,end) = zmid(2:end-1,2);
% else these points don't matter, so leave as zero
end


disp('computing the spline coefficients...')
%  notes for easy manipulation... 9 points form a diamond.
% labeled left to right, top to bottom, we have a,b,c,d,e,f,g,h,i.
% this is memory heavy, maybe make this is a comment, and put in z/zmid forms directly
a = zmid(2:end-1,1:end-2);
b = z;
c = z([2:end 1],:);
d = zmid(1:end-2,2:end-1); 
e = zmid(2:end-1,2:end-1);
f = zmid(3:end,2:end-1);
g = z(:,[2:end 1]);
h = z([2:end 1],[2:end 1]);
i = zmid(2:end-1,3:end);

spl(:,:,1) = b;
spl(:,:,2) = (3*a - 6*b - 2*c + 6*e - i)/(2*dpx);
spl(:,:,3) = (-3*a + 4*b + 4*c - 6*e + i)/(2*dpx*dpx);
spl(:,:,4) = (-6*b + 3*d + 6*e - f - 2*g)/(2*dtx);
spl(:,:,5) = (-4*a + 9*b + 3*c - 4*d - 8*e + 3*g + h)/(dpx*dtx);
spl(:,:,6) = 2*(2*a - 3*b - 3*c + d + 4*e + f - g - h)/(dpx*dpx*dtx);
spl(:,:,7) = (4*b - 3*d - 6*e + f + 4*g)/(2*dtx*dtx);
spl(:,:,8) = 2*(a - 3*b - c + 2*d + 4*e - 3*g - h + i)/(dpx*dtx*dtx);
spl(:,:,9) = -2*(a - 2*b - 2*c + d + 4*e + f - 2*g - 2*h + i)/(dpx*dpx*dtx*dtx);

% for non periodic boundary conditions, zero coefficients on the boundary
% to avoid erratic meaningless crap that makes plots confusing
if ~dim1periodic
    spl(end,:,[2:3 5:6 8:9])=0;
end
if ~dim2periodic
    spl(:,end,4:9)=0;
end

disp('done!')

