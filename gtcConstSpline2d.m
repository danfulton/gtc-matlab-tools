function spl = gtcConstSpline2d(z,dim1size, dim2size)

% toggle for smoothing
smoothing = 0;

% verify user input
% z is two dimensional
if ndims(z)~=2
    error('Input grid must be two dimensional.');
end
% odd number of theta points

% change odd number and first last, not the same or even and same
if mod(size(z,2),2)==0
    error('Input grid must have an odd number of points in 2nd dim.')
end

lsp = size(z,1);
lst = size(z,2);

spl(:,:,1) = z;

dpx = dim1size/(lsp-1);
% again, check here if we have overlap/periodic point
dtx = dim2size/lst;

% spl holds new spline
% format is spl(psi_ind, theta_ind, spline_ind) where spline_ind is 1:9

% here is the inner boundary condition... choose one of these
% original from Orbit code
    spl(1,:,2) = (10*spl(2,:,1) - 7*spl(1,:,1) - 3*spl(3,:,1))/(4*dpx);
% "standard" from discretizing differencing equations
%    spl(1,:,2) = (4*spl(2,:,1) - 3*spl(1,:,1) - spl(3,:,1))/(2*dpx);
% derivative across magnetic axis continuous
%    ind1 = [(ceil(lst/2):lst) (1:floor(lst/2))];
%    ind2 = [ind1(2:end) ind1(1)];
%    spl(1,:,2) = (spl(1,:,1)-0.5*(spl(1,ind1,1)+spl(1,ind2,1)))/(2*dpx);

for j = 2:lsp-2
    spl(j,:,2) = -spl(j-1,:,2) + 2*(spl(j,:,1)-spl(j-1,:,1))/dpx;
    if smoothing    
        % smooth f1
        spl(j+1,:,1) = 0.4*dpx*spl(j,:,2) + 0.3*spl(j+2,:,1) + 0.7*spl(j,:,1);
    end
end
spl(lsp-1,:,2) = -spl(lsp-2,:,2) + 2*(spl(lsp-1,:,1)-spl(lsp-2,:,1))/dpx;

spl(lsp,:,2) = spl(lsp-1,:,2);
% match f3(1,k) at the first flux surface
spl(1:lsp-1,:,3) = (spl(2:lsp,:,2)-spl(1:lsp-1,:,2))/(2*dpx);
spl(lsp-1,:,3) = (spl(lsp,:,1)-spl(lsp-1,:,1)-spl(lsp-1,:,2)*dpx)/(dpx^2);

% f1,f2,f3-finished


% find matrix for f4,f5,f6
    bmat = ldbmat(lst,dtx);
    for j = 1:lsp-1
        % set up RHS (wk2)
        wk2one = 2*spl(j,[2:end 1],1) - 2*spl(j,:,1);
        wk2two = 2*spl(j,[2:end 1],2) - 2*spl(j,:,2);
        wk2three = 2*spl(j,[2:end 1],3) - 2*spl(j,:,3);
        % solve eqn bmat*x=wk2 for vector x
        spl(j,:,4) = gelg(wk2one,bmat);
        spl(j,:,5) = gelg(wk2two,bmat);
        spl(j,:,6) = gelg(wk2three,bmat);
        sp1(j,:,7) = (spl(j,[2:end 1],4) - spl(j,:,4))/(2*dtx);
        spl(j,:,8) = (spl(j,[2:end 1],5) - spl(j,:,5))/(2*dtx);
        spl(j,:,9) = (spl(j,[2:end 1],6) - spl(j,:,6))/(2*dtx);
    end
% f4,f5,f6,f7,f8,f9-finished    

end % function spspline2d


function bmat = ldbmat(lst,dtx)
    bmat = dtx*(diag(ones(lst,1))+diag(ones(lst-1,1),1)+diag(1,1-lst));
end % function bmat


function x = gelg(wk2,bmat) %/,lmax,1,err0,ier)
    if isvector(wk2)
        if isrow(wk2)
            wk2=wk2';
        end
    end

    if size(bmat,1) ~= length(wk2)
        error('Dimensions of bmat and wk2 must match.');
    end
    x = bmat\wk2;
end % function gelg
    
    

