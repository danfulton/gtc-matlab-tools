function pdfprep(fighandle)
% Sets up the 'paperposition' and 'papersize' properties
% of a figure to print a good PDF file from Matlab.
% Accepts a figure handle as an argument.  Lacking this
% defaults to the current figure, specified by 'gcf'.
%
% Example usage:
%   fh = figure;
%   plot(x,y);
%   % ... user resizes figure as desired ...
%   pdfprep(fh)
%   print(fh,'my_xyplot.pdf','-dpdf')
%

if nargin<1
  fighandle=gcf;
end

pos = get(fighandle,'position');
pos = pos/100.0;
pos(1:2) = 0;

set(fighandle,'paperposition',pos);
set(fighandle,'papersize',pos(3:4));

clear fighandle pos

