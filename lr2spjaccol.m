%%%%%%%% jacobian

if diagnostic
% plot jacobian comparison between spdata jacobian 
% and determinant of coordinate gradients
    % want an error plot?
    errplot=1;

    fh = figure;
    set(fh,'position',[40 400 1575 485])
    if flopen
        set(fh,'position',[40 400 1575 950])
    end
    spp = gensubplotpos(2+errplot,1,0,.06,.07,.04,.12,.05);

    for i = 1:size(spp.subplotpos,1) % set subplots with handles
        ha(i) = subplot('position',spp.subplotpos(i,1:4));
        set(ha(i),...
            'fontsize',tickfontsize,...
            'titlefontsizemultiplier',titlefontsize/tickfontsize)
    end

    jaccolrange = [0 2.5];


    %%% compute jacobian from determinant 
    if flclosed
        dxdp = midpts(diff(sp.xsp(1:iLastClosed,:,1),1,1)/(2*pi*sp.psiw/(sp.lsp-1)),1);
        dzdp = midpts(diff(sp.zsp(1:iLastClosed,:,1),1,1)/(2*pi*sp.psiw/(sp.lsp-1)),1);
        dxdt = midpts(diff(sp.xsp(2:iLastClosed-1,[end-1 1:end 2],1),1,2)/(2*pi/(sp.lst-1)),2);
        dzdt = midpts(diff(sp.zsp(2:iLastClosed-1,[end-1 1:end 2],1),1,2)/(2*pi/(sp.lst-1)),2); 
        jacflc = sp.xsp(2:iLastClosed-1,:,1).*...
        (dxdp.*dzdt - dzdp.*dxdt);

        btbflc = sp.bsp(2:iLastClosed-1,:,1)./...
                 sqrt(dxdt.^2 + dzdt.^2);
        divbflc = jacflc.*btbflc;
        divbflc =   midpts(diff(divbflc(:,[end-1 1:end 2]),1,2)/(2*pi/(sp.lst-1)),2);
        divbflc = divbflc./jacflc;
%        divbflc = divbflc./sp.bsp(2:iLastClosed-1,:,1);

        clear dxdp dzdp dxdt dzdt
    end
    if flopen
        dxdp = midpts(diff(sp.xsp(iLastClosed+1:end,:,1),1,1)/(2*pi*sp.psiw/(sp.lsp-1)),1);
        dzdp = midpts(diff(sp.zsp(iLastClosed+1:end,:,1),1,1)/(2*pi*sp.psiw/(sp.lsp-1)),1);
        dxdt = midpts(diff(sp.xsp(iLastClosed+1:end,:,1),1,2)/(2*pi/(sp.lst-1)),2);
        dzdt = midpts(diff(sp.zsp(iLastClosed+1:end,:,1),1,2)/(2*pi/(sp.lst-1)),2); 
        jacflo = sp.xsp(iLastClosed+2:end-1,2:end-1,1).*...
        (dxdp(:,2:end-1).*dzdt(2:end-1,:) - dzdp(:,2:end-1).*dxdt(2:end-1,:));

        btbflo = sp.bsp(iLastClosed+2:end-1,2:end-1,1)./...
                 sqrt(dxdt(2:end-1,:).^2 + dzdt(2:end-1,:).^2);
        divbflo = jacflo.*btbflo;
        divbflo = midpts(diff(divbflo(:,[end-1 1:end 2]),1,2)/(2*pi/(sp.lst-1)),2);
        divbflo = divbflo./jacflo;
%        divbflo = divbflo./sp.bsp(iLastClosed+2:end-1,2:end-1,1);

        clear dxdp dzdp dxdt dzdt
    end

    %%% plot jacobian from physical quantities
    axes(ha(1));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                sp.gsp(2:iLastClosed-1,:,1),...
                'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.gsp(iLastClosed+2:end-1,2:end-1,1),...
                'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    set(gca,'xticklabel','')
    ylabel('R (m)')
    caxis(jaccolrange)
    colorbar
    %title('Jacobian - \mu_0 I_{tor}/4\pi^2 B^2')
    %title('\mu_0 I_{tor}/4\pi^2 B^2',...
    %title('(A)',...
    title('\mu_0 I_{tor}/4\pi^2 B^2',...
          'units','normalized',...
          'position',[.99 .99],...
          'horizontalalignment','right',...
          'verticalalignment','middle')

    %%% plot jacobian from coordinate determinant
    axes(ha(2));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                jacflc,'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                jacflo,'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    if errplot
        set(gca,'xticklabel','')
    else
        xlabel('Z (m)')
    end
    ylabel('R (m)')
    caxis(jaccolrange)
    colorbar
    %title('Jacobian - determinant of coordinate transform')
    %title('det|\mathbf{J}|',...
    %title('(B)',...
    title('det|J|',...
          'units','normalized',...
          'position',[.99 .99],...
          'horizontalalignment','right',...
          'verticalalignment','cap')
        
    % turned this off for now, because it looks bad
    % I believe discrepancy is just related to rough numerics computing finite-diff derterminant
    % and div-by very small B^2.
    %
    %%% divergence of B-field
    axes(ha(3));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                divbflc,...
                'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                divbflo,...
                'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    colorbar
    xlabel('Z (m)')
    ylabel('R (m)')
    caxis([-1 1]*.5e-2)
    colorbar
    title('\nabla\cdotB',...
          'units','normalized',...
          'position',[.99 .99],...
          'horizontalalignment','right',...
          'verticalalignment','cap')
    clear jaccolrange errcolrange ha spp jacflc jacflo
end

