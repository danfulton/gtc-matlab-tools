function A = readspdat(spdata,coord)
% reads an spdata file into a matlab struct, 
% from which it can be easily plotted.
%
% arguments
% spdata: name of spdata.dat file to read in
% coord : 1:boozer + arc edge, 2: boozer

if nargin < 2
	coord = 2; % default coordinate system, boozer
end

% open the spdata file, check for errors
findex = fopen(spdata,'r');
if findex < 0  % error
	error(['Could not open the spdata file ', spdata]);
end

% read data

A.header = fgetl(findex); % header
[A.lsp tmp] = fscanf(findex,'%i',1);
[A.lst tmp] = fscanf(findex,'%i',1);
[A.lemax tmp] = fscanf(findex,'%i',1);
[A.lrmax tmp] = fscanf(findex,'%i',1);
[A.psiw tmp] = fscanf(findex,'%f',1);
[A.ped tmp] = fscanf(findex,'%f',1);

for i=1:A.lsp
	A.bsp(1:A.lst,1:9,i) = fscanf(findex, '%f',[A.lst,9]);
	A.xsp(1:A.lst,1:9,i) = fscanf(findex, '%f',[A.lst,9]);
	A.zsp(1:A.lst,1:9,i) = fscanf(findex, '%f',[A.lst,9]);
	A.gsp(1:A.lst,1:9,i) = fscanf(findex, '%f',[A.lst,9]);
	A.qpsi(1:3,i) = fscanf(findex,'%f',[3,1]);
	A.gpsi(1:3,i) = fscanf(findex,'%f',[3,1]);
	A.rd(1:3,i) = fscanf(findex,'%f',[3,1]);
	A.ppsi(1:3,i) = fscanf(findex,'%f',[3,1]);
	A.rpsi(1:3,i) = fscanf(findex,'%f',[3,1]);
	A.torpsi(1:3,i) = fscanf(findex,'%f',[3,1]);
end

A.bsp = permute(A.bsp,[3 1 2]);
A.xsp = permute(A.xsp,[3 1 2]);
A.zsp = permute(A.zsp,[3 1 2]);
A.gsp = permute(A.gsp,[3 1 2]);

A.qpsi = permute(A.qpsi, [2 1]);
A.gpsi = permute(A.gpsi, [2 1]);
A.rd = permute(A.rd, [2 1]);
A.ppsi = permute(A.ppsi, [2 1]);
A.rpsi = permute(A.rpsi, [2 1]);
A.torpsi = permute(A.torpsi, [2 1]);

A.krip = fscanf(findex, '%4d', 1);
A.nrip = fscanf(findex, '%4d', 1);
A.rmaj = fscanf(findex, '%f', 1);
A.d0 = fscanf(findex, '%f', 1);
A.brip = fscanf(findex, '%f', 1);
A.wrip = fscanf(findex, '%f', 1);
A.xrip = fscanf(findex, '%f', 1);

% close spdata file
fclose(findex);

