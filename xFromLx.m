function x=xFromLx(rn,lx)
% produces a GTC profile quantity x from a numerical vector
% of its length scale expressed as R0/Lx, where
%
%   R0/Lx = - R0*d/dr(ln(x))
%
% The input arguments are rn, the minor radius values (normalized by R0) corresponding
% to the points supplied in the second argument lx.  lx is the inverse length
% scale, normalized to the major radius on the magnetic axis (R0/Lx).
%

if nargin < 2
    error('Function xFromLx requires two input vectors, providing radial')
    error('location in terms of normalized minor radius, r/R0, and')
    error('normalized inverse length scale R0/Lx.')
end

if ~(isvector(rn) & isvector(lx))
    error('Input arguments rn and ln must be vectors')
end

if iscolumn(rn)
    rn = rn';
end

if iscolumn(lx)
    lx = lx';
end

a = [0 midpts(lx).*diff(rn)];
x = exp(-cumsum(a));

clear a
