function A = readLR(h5file,formatid)

% this function is deprecated in favor of the more generic h5ToStruct.m,
% which does not assume any field names

if nargin < 2
  formatid=1;
  if nargin < 1
    error('Function readLR(LRfilename) requires a file name for reading!');
  end
end

switch formatid
  case 1
    A.z = h5read(h5file,'/mesh/z');
    A.r = h5read(h5file,'/mesh/r');
    A.nz = length(A.z);
    A.nr = length(A.r);

    A.bbr = h5read(h5file,'/time0.3ms/Br');
    A.bbz = h5read(h5file,'/time0.3ms/Bz');
    A.bbt = A.bbr*0.0;
    A.psi = h5read(h5file,'/time0.3ms/Psi');

    A.tme = h5read(h5file,'/time0.3ms/Te');
    A.en = h5read(h5file,'/time0.3ms/nelec');
    A.tm01 = h5read(h5file,'/time0.3ms/Ti');
    A.en01 = h5read(h5file,'/time0.3ms/nions');

  case 2
    A.ncute = h5read(h5file,'/mesh/n_cute');
    A.nr = h5read(h5file,'/mesh/nr');
    A.nz = h5read(h5file,'/mesh/nz');
    A.r = h5read(h5file,'/mesh/r');
    A.rb = h5read(h5file,'/mesh/r_b');
    A.rvert = h5read(h5file,'/mesh/r_vert');
    A.vkind = h5read(h5file,'/mesh/v_kind');
    A.z = h5read(h5file,'/mesh/z');
    A.zb = h5read(h5file,'/mesh/z_b');
    A.zvert = h5read(h5file,'/mesh/z_vert');
    
    A.bbr = h5read(h5file,'/state/bbr');
    A.bbt = h5read(h5file,'/state/bbt');
    A.bbz = h5read(h5file,'/state/bbz');
    A.eer = h5read(h5file,'/state/eer');
    A.eez = h5read(h5file,'/state/eez');
    A.en = h5read(h5file,'/state/en');
    A.en01 = h5read(h5file,'/state/en01');
    A.ome = h5read(h5file,'/state/ome');
    A.omi = h5read(h5file,'/state/omi');
    A.tm01 = h5read(h5file,'/state/tm01');
    A.tme = h5read(h5file,'/state/tme');
    A.wik = h5read(h5file,'/state/wik');
    
    A.jt = h5read(h5file,'/vacuum/jt');
    A.psf = h5read(h5file,'/vacuum/psf');
    A.psi = h5read(h5file,'/vacuum/psi');

  otherwise
    disp('Invalid formatid in readLR() subroutine.');
end


