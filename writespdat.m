function writespdat(sp,fname)
% write an spdata file from a matlab struct, 
%
% arguments
% sp: handle identifying spdata matlab struct
% fname : string containing path to spdata file to be written.
%         will default to 'spdata.dat' in the local directory
%         if not specified.

if nargin < 2
    if nargin < 1
        error('Must supply a struct handle to write spdata.');
    end
	fname = 'spdata.dat'; % default output file name
end

disp(['Output file: ' fname]);

% open the spdata file for writing, check for errors
findex = fopen(fname,'w');
if findex < 0  % error
	error(['Could not open the spdata file ', spdata]);
end

%%%% write data

% header
fprintf(findex, '%c', sp.header);
fprintf(findex, '\n');

% some scalar file metadata
fprintf(findex, '%4d%4d%4d%4d\n', [sp.lsp sp.lst sp.lemax sp.lrmax]);

% polodal flux on wall
%[A.psiw tmp] = fscanf(findex,'%f',1);
%[A.ped tmp] = fscanf(findex,'%f',1);
fprintf(findex, '%18.10E%18.10E\n', [sp.psiw sp.ped]);

% produce format string for writing...
% 2D splines
format2D = [ ...
            repmat('%18.10E%18.10E%18.10E%18.10E\n',1,floor(sp.lst/4)) ...
            repmat('%18.10E',1,mod(sp.lst,4)) ...
            '\n' ...
            ];
% 1D splines            
format1D = '%18.10E%18.10E%18.10E\n';
            

% write the body of data to file
% for each poloidal flux surface...
for i=1:sp.lsp
    % 2D flux quantities
    fprintf(findex, format2D, sp.bsp(i,:,:));
    fprintf(findex, format2D, sp.xsp(i,:,:));
    fprintf(findex, format2D, sp.zsp(i,:,:));
    fprintf(findex, format2D, sp.gsp(i,:,:));

    % 1D quantities
    fprintf(findex, format1D, sp.qpsi(i,:));
    fprintf(findex, format1D, sp.gpsi(i,:));
    fprintf(findex, format1D, sp.rd(i,:));
    fprintf(findex, format1D, sp.ppsi(i,:));
    fprintf(findex, format1D, sp.rpsi(i,:));
    fprintf(findex, format1D, sp.torpsi(i,:));
end

fprintf(findex, '%4d%4d\n', [sp.krip sp.nrip]);
fprintf(findex, '%18.10E%18.10E%18.10E\n', [sp.rmaj sp.d0 sp.brip]);
fprintf(findex, '%18.10E%18.10E\n', [sp.wrip, sp.xrip]);

% close spdata file
fclose(findex);

% done!
