function sp2prof(spdata, profile, coord)

% Creates a profile.dat file from an spdata.dat file.
% Arguments:
% spdata: name of the spdata.dat file to be read.
% profile: name of the profile.dat file to be created.
% coord: coordinate system 1:boozer plus equal arc; 2: boozer only

%%% Set default variables and open spdata file. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 3
	coord = 2;  %default is boozer only
	if nargin < 2
		profile = 'profile.dat'; % default output file
	end
end

% read in the spdata.dat file
a = readspdat(spdata,coord);

%%% Profiles Set Here %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Modify this to produce a different profile.  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dummy=0.0;

%{
%value of grad T profile at each r
tmp = exp(-(((a.rpsi(1,:)/0.36)-0.5)/0.28).^6.0);
%value of integral elements at each r
tmp2 = (a.rpsi(1,2:end)-a.rpsi(1,1:end-1)).*(tmp(1:end-1)+tmp(2:end))/2;


%on axis values
polflux(1) = 1e-20;
dum(1) = dummy;
T(1) = 1.0;
n(1) = 1.0;

% values are regular in poloidal flux, dum is a dummy/filler,...
% T and n are determined by evaulating the integral at each r val and exponentiating.
for j = 2:a.lsp
polflux(j) = (j-1)*a.psiw/(a.lsp-1.0)+1e-20;
dum(j) = dummy;
T(j) = exp(-6.9*sum(tmp2(1:j-1)));
n(j) = exp(-2.2*sum(tmp2(1:j-1)));
end
%}

for j = 2:a.lsp
    polflux(j) = (j-1)*a.psiw/(a.lsp-1.0)+1e-20;
    dum(j) - dummy;
    T(j) 
end

%%% Write to Output File %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% open the profile.dat file to write to.
findex = fopen(profile,'w');

% write to file

%header
fprintf(findex, '   Pol-Flux            x              r              R               R+r             Te              ne              Ti            Zeff          omega-tor           Er              ni           nimp             nf              Tf\n');

%data
fprintf(findex, '%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t%e\t\n', [polflux; zeros(1,a.lsp); a.rpsi(1,:); zeros(1,a.lsp); zeros(1,a.lsp); T; n; T; zeros(1,a.lsp); zeros(1,a.lsp); zeros(1,a.lsp); zeros(1,a.lsp); zeros(1,a.lsp); zeros(1,a.lsp); zeros(1,a.lsp)]);

%footer
fprintf(findex,'\n');
fprintf(findex,'Poloidal Flux in Webers\n');
fprintf(findex,'x is square root of normalized toroidal flux\n');
fprintf(findex,'r is midplane radius in cm (midplane halfwidth of flux surface width at midplane)\n');
fprintf(findex,'R is flux surface center in cm\n');
fprintf(findex,'R+r is local major radius in cm on the outer midplane\n');
fprintf(findex,'Te is electron temperature in eV\n');
fprintf(findex,'ne is electron density in m^-3\n');
fprintf(findex,'Ti is ion temperature in eV; last two points are artificial\n');
fprintf(findex,'Zeff is from Carbon density profile measurement\n');
fprintf(findex,'omega-tor is measured angular velocity from carbon rotation in radians/sec;to get votr, multiply omega by local R from equilibrium (or from R+r)\n');
fprintf(findex,'Er is radial electric field in volts/cm\n');
fprintf(findex,'ni is ion density in m^-3\n');
fprintf(findex,'nimp is impurity density in 10^19 m^-3\n');
fprintf(findex,'nf is fast ion density in m^-3\n');
fprintf(findex,'Tf is electron temperature in eV\n');

% close profile.dat file
fclose(findex);


