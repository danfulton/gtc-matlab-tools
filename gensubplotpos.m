function a = gensubplotpos(nrows,ncols,hspace,vspace,left,right,bottom,top)
% function gensubplotpos (nrows,ncols,hspace,vspace,left,right,bottom,top)
% manually generates position vectors for close cropped subplots.
% As input parameters, takes a number of rows and number of columns,
% vertical and horizontal space, left, right, bottom, and top margins.
% Arguements space, left, and right are specified as a fraction of the 
% subplot width, e.g. 0.05 for 5% of the subplot width.  Arguments bottom
% and top are specified in terms of subplot height.
%
% Returns a struct with some meta information,including user input
% (which may be needed for text placement, etc) and useable position
% vectors which should be passed as arguments to subplot.  The
% 'pos' field of the returned struct is a function to access
% positions by index, while position data is stored in
% 'subplotpos'.
%
% Example usage:
%   figure('position',[100 100 1225 675]) % fairly elongated figure, vert %s need to be bigger
%   p = gensubplotpos(2,1,...             % 2x1 subplot
%                     0,.03,...           % no horizontal space (unused), 3% vertical space
%                     .05,.01,...         % 5% space on left margin (for labels), 1% space on right
%                     .1,.01);            % 10% space on bottom margin (labels0, 1% space on top
%   subplot('position',p.pos(1,1))
%   contourf(x,y,z)
%   subplot('position',p.pos(2,1))
%   plot(t,sin(t))

a.m=nrows;
a.n=ncols;

a.dx = (1.0-left-right-hspace*(ncols-1))/ncols;
a.dy = (1.0-top-bottom-vspace*(nrows-1))/nrows;
a.hspace=hspace;
a.vspace=vspace;
a.left=left;
a.right=right;
a.top=top;
a.bottom=bottom;

for i = 1:nrows
    for j = 1:ncols
        clear tmppos
        tmppos(1) = left+(j-1)*(a.dx+hspace);
%        tmppos(2) = bottom+(i-1)*(a.dy+vspace);
        tmppos(2) = bottom+(nrows-i)*(a.dy+vspace);
        tmppos(3) = a.dx;
        tmppos(4) = a.dy;
        a.subplotpos((i-1)*ncols+j,1:4)= tmppos;
    end
end
        
a.pos = @(in,im)a.subplotpos((im-1)*a.n+in,:);

%        a.subplotpos(1:4,i,j)=[(left*dx+(j-1)*(dx+absspace))/width (bottom*dy+(nrows-i)*(dy+absspace))/height dx/width dy/%height];
%    end
%end



%a.xlim = get(ah,'xlim');
%a.ylim = get(ah,'ylim');

%dx = diff(a.xlim);
%dy = diff(a.ylim);
%absspace = space*dx;

%width = (ncols+left+right)*dx + +(ncols-1)*absspace;
%height = (nrows+top+bottom)*dy + (nrows-1)*absspace;

%if height/width > 0.8
%    a.figpos = [300 150 800 floor(800*width/height)];
%else
%    a.figpos = [300 150 floor(1000*height/width) 1000];
%end

%a.paperpos = (a.figpos/a.figpos(4))*(5.0*height/dy);

%for i = 1:nrows
%    for j = 1:ncols
%        a.subplotpos(1:4,i,j)=[(left*dx+(j-1)*(dx+absspace))/width (bottom*dy+(nrows-i)*(dy+absspace))/height dx/width dy/%height];
%    end
%end

