function frames = gtcModeStrucMovie(outputMovieFile, framerate, showmov)

nLvlsContourPlot = 20;

if nargin < 3
  if nargin < 2
    if nargin < 1
        % default output file name
        outputMovieFile = 'tmpSnapshotMovie.avi';
    end
    framerate = 2;
  end
  showmov=1;
end

disp('determining snapshot files...')
snapfiles = dir('snap*.out');
disp(['there are ' num2str(length(snapfiles)) ' snapshot files:'])
dir('snap*.out')

disp('initializing figure to capture movie frames...')

% determine approximate aspect ratio of figure
fh = figure;
ah = axes;
sn = gtcReadSnap(snapfiles(1).name);
contourf(sn.x,sn.y,sn.phipolplane,nLvlsContourPlot,'linestyle','none');
set(ah,'dataaspectratio',[1,1,1])
ap = get(ah,'position');
close(fh)
clear sn fh ah

% create a figure to plot in, and set to predetermined size
fh = figure;
set(fh,'position',[300 50 1000*ap(3)/ap(4) 900]);
% axes in figure, and makesure data aspect ratio is correct
ah = axes;
% use idl-like colors
idl = load('idl.colormap');
colormap(idl);

% initialize the video file to write to
writeObj = VideoWriter(outputMovieFile);
writeObj.FrameRate = framerate;
open(writeObj);

disp(['capturing frames and saving to ' outputMovieFile '...'])
for i=1:length(snapfiles)
    disp([num2str(i) ' of ' num2str(length(snapfiles))]);
    sn = gtcReadSnap(snapfiles(i).name);
    contourf(sn.x,sn.y,sn.phipolplane,nLvlsContourPlot,'linestyle','none');
    set(ah,'dataaspectratio',[1 1 1])
    colorbar
    frames(i) = getframe(fh);
    writeVideo(writeObj,frames(i));
    clear sn
end

close(writeObj);

disp(['frames saved to: ' outputMovieFile])

if showmov
  disp('play the movie...')

  % create the figure, and set it to be the same size as our plot figure
  fh2 = figure;
  set(fh2,'position',[500 50 1000*ap(3)/ap(4) 900])
  % make our plot axes take up the entire figure. turn off all of the tick marks, etc.
  ah2 = axes;
  a2op = get(ah2,'OuterPosition');
  set(ah2,'position',a2op);
  axis(ah2,'off')

  % finally, plot the movie on the axes!
  movie(frames,3,framerate)
end

disp('done!')
