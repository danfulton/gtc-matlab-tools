%%%%%%%% jacobian

if diagnostic
% plot jacobian comparison between spdata jacobian 
% and determinant of coordinate gradients
    % want an error plot?
    errplot=1;

%    jaccolrange = [0 1e-2];
    jaccolrange = [0 2.5];
    errcolrange = [-1 1]*.3;

    %%% compute jacobian from determinant 
    if flclosed
        dxdp = midpts(diff(sp.xsp(1:iLastClosed,:,1),1,1)/(2*pi*sp.psiw/(sp.lsp-1)),1);
        dzdp = midpts(diff(sp.zsp(1:iLastClosed,:,1),1,1)/(2*pi*sp.psiw/(sp.lsp-1)),1);
        dxdt = midpts(diff(sp.xsp(2:iLastClosed-1,[end-1 1:end 2],1),1,2)/(2*pi/(sp.lst-1)),2);
        dzdt = midpts(diff(sp.zsp(2:iLastClosed-1,[end-1 1:end 2],1),1,2)/(2*pi/(sp.lst-1)),2); 
        jacflc = sp.xsp(2:iLastClosed-1,:,1).*...
        (dxdp.*dzdt - dzdp.*dxdt);%.*...
%        sp.bsp(2:iLastClosed-1,:,1).^2;

        btbflc = sp.bsp(2:iLastClosed-1,:,1)./...
                 sqrt(dxdt.^2 + dzdt.^2);
        divbflc = jacflc.*btbflc;
        divbflc =   midpts(diff(divbflc(:,[end-1 1:end 2]),1,2)/(2*pi/(sp.lst-1)),2);
        divbflc = divbflc./jacflc;

        aflc = sp.xsp(2:iLastClosed-1,:,1);
        bflc = sp.bsp(2:iLastClosed-1,:,1).^2;
        cflc = sp.xsp(2:iLastClosed-1,:,1).*...
               sp.bsp(2:iLastClosed-1,:,1).^2;
        dflc = dxdp.*dzdt;
        fflc = dzdp.*dxdt;

        clear dxdp dzdp dxdt dzdt
    end
    if flopen
        dxdp = midpts(diff(sp.xsp(iLastClosed+1:end,:,1),1,1)/(2*pi*sp.psiw/(sp.lsp-1)),1);
        dzdp = midpts(diff(sp.zsp(iLastClosed+1:end,:,1),1,1)/(2*pi*sp.psiw/(sp.lsp-1)),1);
        dxdt = midpts(diff(sp.xsp(iLastClosed+1:end,:,1),1,2)/(2*pi/(sp.lst-1)),2);
        dzdt = midpts(diff(sp.zsp(iLastClosed+1:end,:,1),1,2)/(2*pi/(sp.lst-1)),2); 
        jacflo = sp.xsp(iLastClosed+2:end-1,2:end-1,1).*...
        (dxdp(:,2:end-1).*dzdt(2:end-1,:) - dzdp(:,2:end-1).*dxdt(2:end-1,:));%.*...
%        sp.bsp(iLastClosed+2:end-1,2:end-1,1).^2;

        btbflo = sp.bsp(iLastClosed+2:end-1,2:end-1,1)./...
                 sqrt(dxdt(2:end-1,:).^2 + dzdt(2:end-1,:).^2);
        divbflo = jacflo.*btbflo;
        divbflo =   midpts(diff(divbflo(:,[end-1 1:end 2]),1,2)/(2*pi/(sp.lst-1)),2);
        divbflo = divbflo./jacflo;


        aflo = sp.xsp(iLastClosed+2:end-1,2:end-1,1);
        bflo = sp.bsp(iLastClosed+2:end-1,2:end-1,1).^2;
        cflo = sp.xsp(iLastClosed+2:end-1,2:end-1,1).*...
               sp.bsp(iLastClosed+2:end-1,2:end-1,1).^2;
        dflo = dxdp(:,2:end-1).*dzdt(2:end-1,:); 
        fflo = dzdp(:,2:end-1).*dxdt(2:end-1,:);

        clear dxdp dzdp dxdt dzdt
    end

    %%%%% set up first figure
    fh = figure;
    set(fh,'position',[40 400 1575 485])
    if flopen
        set(fh,'position',[40 400 1575 700+250*errplot])
    end
    spp = gensubplotpos(2+errplot,1,0,.06,.07,.04,.12,.05);

    for i = 1:size(spp.subplotpos,1) % set subplots with handles
        ha(i) = subplot('position',spp.subplotpos(i,1:4));
        set(ha(i),...
            'fontsize',tickfontsize,...
            'titlefontsizemultiplier',titlefontsize/tickfontsize)
    end

    %%% plot jacobian from physical quantities
    axes(ha(1));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                sp.gsp(2:iLastClosed-1,:,1),...%.*sp.bsp(2:iLastClosed-1,:,1).^2,...
                'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.gsp(iLastClosed+2:end-1,2:end-1,1),...%.*sp.bsp(iLastClosed+2:end-1,2:end-1,1).^2,...
                'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    set(gca,'xticklabel','')
    ylabel('R (m)')
    caxis(jaccolrange)
    colorbar
    %title('Jacobian - \mu_0 I_{tor}/4\pi^2 B^2')
    %title('\mu_0 I_{tor}/4\pi^2 B^2',...
    %title('(A)',...
    title('\mu_0 I_{tor}/4\pi^2',...
          'units','normalized',...
          'position',[.99 .99],...
          'horizontalalignment','right',...
          'verticalalignment','middle')

    %%% plot jacobian from coordinate determinant
    axes(ha(2));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                jacflc,'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                jacflo,'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    if errplot
        set(gca,'xticklabel','')
    else
        xlabel('Z (m)')
    end
    ylabel('R (m)')
    caxis(jaccolrange)
    colorbar
    %title('Jacobian - determinant of coordinate transform')
    %title('det|\mathbf{J}|',...
    %title('(B)',...
    title('det|J|*B^2',...
          'units','normalized',...
          'position',[.99 .99],...
          'horizontalalignment','right',...
          'verticalalignment','cap')
        
    % turned this off for now, because it looks bad
    % I believe discrepancy is just related to rough numerics computing finite-diff derterminant
    % and div-by very small B^2.
    %
    %%% percent difference between two jacobians
    if errplot
        axes(ha(3));
        if flclosed
            surface(sp.zsp(2:iLastClosed-1,:,1),...
                    sp.xsp(2:iLastClosed-1,:,1),...
                    100*(jacflc - sp.gsp(2:iLastClosed-1,:,1).*sp.bsp(2:iLastClosed-1,:,1).^2)./jacflc,...
                    'linestyle','none')
            xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
            ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
        end
        if flopen
            hold on
            surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                    sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                    100*(jacflo - sp.gsp(iLastClosed+2:end-1,2:end-1,1).*sp.bsp(iLastClosed+2:end-1,2:end-1,1).^2)./jacflo,...
                    'linestyle','none')
            xlim([-1 1]*zbound)
            ylim([0 0.6])
        end
        xlabel('Z (m)')
        ylabel('R (m)')
%        caxis(errcolrange*100)
        colorbar
%       %title('Jacobian error (Jac. fig.2 - Jac. fig.1)/(Jac. fig.2)')
%       title('(C)',...
        title('% err',...
              'units','normalized',...
              'position',[.99 .99],...
              'horizontalalignment','right',...
              'verticalalignment','cap')
    end

    %%%%% set up second figure for plots
    fh = figure;
    set(fh,'position',[70 300 1900 800])
    clear spp
    spp = gensubplotpos(4,2,.02,.06,.07,.04,.12,.05);

    for i = 1:size(spp.subplotpos,1) % set subplots with handles
        ha2(i) = subplot('position',spp.subplotpos(i,1:4));
    end
    %%% a
    axes(ha2(1));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                aflc,'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                aflo,'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    colorbar
    title('xsp')
    %%% b
    axes(ha2(2));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                bflc,'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                bflo,'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    colorbar
    title('bsp^2')
    %%% c
    axes(ha2(3));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                cflc,'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                cflo,'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    colorbar
    title('xsp*bsp^2')
    %%% d
    axes(ha2(4));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                dflc,'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                dflo,'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    colorbar
    title('\partial x\partial\psi \cdot \partial z\partial\theta')
    %%% f
    axes(ha2(5));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                fflc,'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                fflo,'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    colorbar
    title('\partial x\partial\theta \cdot \partial z\partial\psi')
    %%% d - f
    axes(ha2(6));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                dflc-fflc,'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                dflo-fflo,'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    colorbar
    title('\partialx\partial\psi \cdot \partialz\partial\theta - \partialx\partial\theta \cdot \partialz\partial\psi')
    %%% 1/(2*pi*B)
    axes(ha2(7));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                1./(2*pi*sqrt(bflc)),'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                1./(2*pi*sqrt(bflo)),'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    colorbar
    title('1/(2\pibsp)')
    %%% jacobian
    axes(ha2(8));
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                sp.gsp(2:iLastClosed-1,:,1),...
                'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.gsp(iLastClosed+2:end-1,2:end-1,1),...
                'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    set(gca,'xticklabel','')
    colorbar
    title('jacobian - gsp')

    %%% jacobian*B_thetaB
    figure
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                jacflc.*btbflc,...
                'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                jacflo.*btbflo,...
                'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    set(gca,'xticklabel','')
    colorbar
    title('J\cdotB^{\theta_B}')

    %%% jacobian*B_thetaB
    figure
    if flclosed
        surface(sp.zsp(2:iLastClosed-1,:,1),...
                sp.xsp(2:iLastClosed-1,:,1),...
                divbflc,...
                'linestyle','none')
        xlim([-1 1]*1.05*max(sp.zsp(iLastClosed-1,:,1)))
        ylim([0 1.05*max(sp.xsp(iLastClosed-1,:,1))])
    end
    if flopen
        hold on
        surface(sp.zsp(iLastClosed+2:end-1,2:end-1,1),...
                sp.xsp(iLastClosed+2:end-1,2:end-1,1),...
                divbflo,...
                'linestyle','none')
        xlim([-1 1]*zbound)
        ylim([0 0.6])
    end
    set(gca,'xticklabel','')
    colorbar
    title('\nabla\cdot\vec{B}')


    clear jaccolrange errcolrange ha spp jacflc jacflo
end



