function dadb = dAdB(A,B)
%Returns simple numerical derivative of A in terms of B, assuming A and B are vectors of equal length

if nargin ~= 2
  error('Input arguments must be two vectors of equal length, dAdB(A,B).')
elseif isvector(A)==0
  error('First argument, A, must be a vector.')
elseif isvector(B)==0
  error('Second argument, B, must be a vector.')
elseif length(A)~= length(B)
  error('Vectors A and B must have equal length.')
end

tmp = midpts(diff(A)./diff(B));
tmp = [tmp(1)-0.5*diff(B(1:2))*(diff(tmp(1:2))/midpts(diff(B(1:3)))) tmp];
tmp(end+1) = tmp(end)+0.5*diff(B(end-1:end))*(diff(tmp(end-1:end))/midpts(diff(B(end-2:end))));

dadb = tmp;
