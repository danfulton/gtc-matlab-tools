function A = h5ToStruct(h5file)

if nargin < 1
    error('Function h5ToStruct(h5file) requires a file name for reading!');
end

clear A;

% get info about the hdf5 files structure
info = h5info(h5file);

% we assume the structure is fairly flat here
% i.e. groups
%           datasets
% and that all dataset names are unique
for i=1:length(info.Groups)
    for j=1:length(info.Groups(i).Datasets)
        % get the dataset name
        %   this is used in the string to read the field
        %   as well as to create the fieldname in the output struct
        attr=info.Groups(i).Datasets(j).Name;
        % create the read string to access hdf5 file
        str=[info.Groups(i).Name '/' attr];
        % strip underscores (_) out of the attribute name, because icky
        attr = attr(find(attr~='_'));
        % make attribute name lowercase
        attr = lower(attr);
        A.(attr) = h5read(h5file,str);
    end
end

clear info
