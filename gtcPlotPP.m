eq = gtcReadEq('equilibrium.out')
%vertical line x location
vlx =0.945;
%window height factor
whf = 3.5;
%x range
xrange=[.8 1];
figure

subplot(2,3,1)
hold on
plot(eq.psi/eq.psi(end),eq.ni,'-r')
plot(eq.psi/eq.psi(end),eq.ne,'-b')
%clear tmp; tmp = ylim;
%ylim([tmp(1), whf*interp1(eq.psi/eq.psi(end),eq.ne,vlx)])
title('number density (m^{-3})')
xlabel('\psi_n')
xlim(xrange)
clear tmp; tmp=ylim;
plot([vlx vlx],ylim,'--k')
ylim(tmp)

subplot(2,3,2)
hold on
plot(eq.psi/eq.psi(end),eq.Ti,'-r')
plot(eq.psi/eq.psi(end),eq.Te,'-b')
clear tmp; tmp = ylim;
ylim([tmp(1), whf*interp1(eq.psi/eq.psi(end),eq.Te,vlx)])
plot([vlx vlx],ylim,'--k')
title('Temp (eV)')
xlabel('\psi_n')
xlim(xrange)

subplot(2,3,3)
hold on
plot(eq.psi/eq.psi(end),eq.p,'-g')
clear tmp; tmp = ylim;
ylim([tmp(1), whf*interp1(eq.psi/eq.psi(end),eq.p,vlx)])
plot([vlx vlx],ylim,'--k')
title('Pressure')
xlabel('\psi_n')
xlim(xrange)

subplot(2,3,4)
hold on
plot(eq.psi/eq.psi(end),eq.dlnnidpsi,'-r')
plot(eq.psi/eq.psi(end),eq.dlnnedpsi,'-b')
plot([vlx vlx],ylim,'--k')
title('-d ln(n)/dr')
xlabel('\psi_n')
xlim(xrange)

subplot(2,3,5)
hold on
plot(eq.psi/eq.psi(end),eq.dlnTidpsi,'-r')
plot(eq.psi/eq.psi(end),eq.dlnTedpsi,'-b')
plot([vlx vlx],ylim,'--k')
title('-d ln(T)/dr')
xlabel('\psi_n')
xlim(xrange)

subplot(2,3,6)
hold on
plot(eq.psi/eq.psi(end),eq.q,'-g')
title('q')
xlabel('\psi_n')
xlim(xrange)
plot([vlx vlx],ylim,'--k')
