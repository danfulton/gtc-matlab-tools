function spl = gtcConstSpline1d(y,dimsize)

lsp = length(y);

% given points
spl(:,1) = y;

dpx = dimsize/(lsp-1);

% spl holds new spline
% format is spl(psi_ind, spline_ind) where spline_ind is 1:3

% inner boundary condition... educated guess of slope
spl(1,2) = (2*spl(2,1) - 1.5*spl(1,1) - 0.5*spl(3,1))/dpx;

for i = 1:lsp-1
    spl(i,3) = (spl(i+1,1)-spl(i,1)-spl(i,2)*dpx)/(dpx*dpx);
    spl(i+1,2) = spl(i,2)+2*spl(i,3)*dpx;
end

