function y = gaussian(a,b,c,xrange)
% function gaussian produces a vector filled with gaussian points y(x), where
% the form of the gaussian bell curve is 
%
%   y(x) = a*exp(-((x-b).^2)/(2*c^2))
%
% User may supply the desired x values in the variable 'xrange'.  If no 
% user defined values are suppled, gaussian will use xrange=[0:0.01:1];
%

if nargin > 4
    error('Too many arguments.  Try help gaussian.')
end

if nargin < 3
    error('Not enough arguments.  Try help gaussian.')
end

if nargin < 4
    xrange=[0:0.01:1];
end

y = a*exp(-((xrange-b).^2)/(2*c^2));
