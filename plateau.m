function y = plateau(a,b,c,d,xrange)
% function y = plateau(a,b,c,d,xrange)
%
% function plateau produces a vector filled with plateau points y(x), where
% the form of the plateau is
%
%   y(x) = a*(tanh(20*(x-b+c/2)/c)-tanh(20*(x-b-c/2)/c))/2
%
% That is...
%   a is the plateau amplitude (max height)
%   b is the plateau center
%   c is the plateau width
%   d is the rise width (narrow width means faster transition from zero to plateau top)
%
% User may supply the desired x values in the variable 'xrange'.  If no 
% user defined values are suppled, gaussian will use xrange=[0:0.01:1];
%

if nargin > 5
    error('Too many arguments.  Try help plateau.')
end

if nargin < 3
    error('Not enough arguments.  Try help plateau.')
end

if nargin < 4
    xrange=[0:0.01:1];
end

%y = a*(tanh(20*(xrange-b+c/2)/c)-tanh(20*(xrange-b-c/2)/c))/2;

y = a*(tanh(20*(xrange-b+c/2)/(c*d))-tanh(20*(xrange-b-c/2)/(c*d)))/2;
